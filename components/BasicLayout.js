import Masthead from "components/Masthead";
import Footer from "components/Footer";

export default function BasicLayout({ children }) {
  return (
    <>
      <Masthead />
      {children}
      <Footer />
    </>
  );
}
