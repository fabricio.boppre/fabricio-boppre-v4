import { useRouter } from "next/router";
import { t } from "../lib/localization/localization";
import translationsLibrary from "../lib/localization/places/anotacoes_de_leitura.json";

export default function ReadingNote(props) {
  const router = useRouter();
  const { locale } = router;

  var date;
  if (props.note.date == null) {
    date =
      "<p>(" + t(translationsLibrary, locale, "credit_date_null") + ")</p>";
  } else {
    date =
      "<p>(" +
      t(translationsLibrary, locale, "credit_date_label") +
      " " +
      props.note.date +
      ")</p>";
  }
  return (
    <li>
      <a className="anchor" id={props.note.id}>
        <h2>{props.note.title}</h2>
      </a>
      <div
        className="note"
        dangerouslySetInnerHTML={{ __html: props.note.content }}
      />
      <div
        className="info"
        dangerouslySetInnerHTML={{ __html: props.note.credit + date }}
      />
    </li>
  );
}
