import styles from "styles/IncompatibleBrowser.module.css";
import { useRouter } from "next/router";
import { t } from "lib/localization/localization";
import translationsLibrary from "lib/localization/places/incompatible_browser.json";

export default function IncompatibleBrowser() {
  const router = useRouter();
  const { locale } = router;

  return (
    <div id={styles.incompatible_browser}>
      <p>{t(translationsLibrary, locale, "message")}</p>
    </div>
  );
}
