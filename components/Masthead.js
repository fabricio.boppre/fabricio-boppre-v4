import Link from "next/link";
import styles from "styles/Masthead.module.css";

export default function Masthead() {
  return (
    <header id={styles.masthead}>
      <div id="name">
        <Link href="/">
          <a>fabricio c. boppré</a>
        </Link>
      </div>
      <div id="occupation">web developer</div>
      <div id="contact">
        <a href="mailto:fabricio.boppre@gmail.com">fabricio.boppre@gmail.com</a>
      </div>
    </header>
  );
}
