import styles from "styles/Footer.module.css";
import Link from "next/link";
import { useRouter } from "next/router";
import LanguagesList from "components/LanguagesList";
import { t } from "lib/localization/localization";
import translationsLibrary from "lib/localization/places/footer.json";

export default function Footer() {
  const router = useRouter();
  const { locale } = router;

  return (
    <footer id={styles.footer}>
      <ul>
        <li>
          <Link href="/perfis_e_redes_sociais">
            <a>
              {t(
                translationsLibrary,
                locale,
                "perfis_e_redes_sociais_link_title"
              )}
            </a>
          </Link>
        </li>
        <li>
          <Link href="/sobre_este_site">
            <a>
              {t(translationsLibrary, locale, "sobre_este_site_link_title")}
            </a>
          </Link>
        </li>
        <li>
          <a
            href="https://deletefacebook.com/"
            target="_blank"
            rel="noreferrer"
          >
            delete facebook
          </a>
        </li>
        <li>
          <LanguagesList />
        </li>
      </ul>
    </footer>
  );
}
