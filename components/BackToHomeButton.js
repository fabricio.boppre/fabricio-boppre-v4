import styles from "styles/BackToHomeButton.module.css";
import Link from "next/link";
import { useRouter } from "next/router";
import { t } from "lib/localization/localization";
import translationsLibrary from "lib/localization/places/back_to_home_button.json";

export default function BackToHomeButton() {
  const router = useRouter();
  const { locale } = router;

  return (
    <Link href="/">
      <a id={styles.back_to_home_button}>
        &#8592; {t(translationsLibrary, locale, "button_text")}
      </a>
    </Link>
  );
}
