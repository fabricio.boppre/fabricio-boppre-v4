import Head from "next/head";
import BasicLayout from "components/BasicLayout";
import BackToHomeButton from "components/BackToHomeButton";
import { useRouter } from "next/router";
import { NextSeo } from "next-seo";
import { t } from "lib/localization/localization";
import translationsLibrary from "lib/localization/places/404.json";
import styles from "styles/Page.module.css";

export default function Custom404() {
  const router = useRouter();
  const { locale } = router;

  return (
    <>
      <NextSeo 
        noindex={true}
        nofollow={true}
        title= "fabricio c. boppré"
      />

      <main id={styles.page} className="content">
        <article>
          <h1>Oops.</h1>
          <p>{t(translationsLibrary, locale, "message")}</p>
        </article>

        <BackToHomeButton />
      </main>
    </>
  );
}

Custom404.getLayout = function getLayout(page) {
  return <BasicLayout>{page}</BasicLayout>;
};
