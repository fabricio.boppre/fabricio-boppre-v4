import Head from "next/head";
import BasicLayout from "components/BasicLayout";
import BackToHomeButton from "components/BackToHomeButton";
import { useRouter } from "next/router";
import { t } from "lib/localization/localization";
import translationsLibrary from "lib/localization/places/error.json";
import styles from "styles/Page.module.css";

export default function Error({ statusCode }) {
  const router = useRouter();
  const { locale } = router;

  return (
    <>
      <Head>
        <title>fabricio c. boppré</title>
      </Head>

      <main id={styles.page} className="content">
        <article>
          <h1>Oops.</h1>
          {statusCode ? (
            <p>
              {t(translationsLibrary, locale, "server_error_message")}$
              {statusCode}:{" "}
            </p>
          ) : (
            <p>{t(translationsLibrary, locale, "browser_error_message")}</p>
          )}
        </article>

        <BackToHomeButton />
      </main>
    </>
  );
}

Error.getInitialProps = ({ res, err }) => {
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404;
  return { statusCode };
};

Error.getLayout = function getLayout(page) {
  return <BasicLayout>{page}</BasicLayout>;
};
