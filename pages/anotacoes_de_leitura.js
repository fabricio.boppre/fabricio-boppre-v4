import React, { useState } from "react";
import BasicLayout from "components/BasicLayout";
import BackToHomeButton from "components/BackToHomeButton";
import ReadingNote from "components/ReadingNote";
import { generateRSS, getAnotacoesDeLeitura } from "lib/anotacoes_de_leitura";
import { useRouter } from "next/router";
import { NextSeo } from "next-seo";
import { t } from "lib/localization/localization";
import translationsLibrary from "lib/localization/places/anotacoes_de_leitura.json";
import styles from "styles/AnotacoesDeLeitura.module.css";

export default function AnotacoesDeLeitura({ notesData }) {
  const router = useRouter();
  const { locale, pathname } = router;

  const feedURL = process.env.basicURL + "feed-anotacoes-de-leitura-" + locale + ".xml";

  // These constants and state controls the number of notes being shown:
  const notesPerPage = 10;
  const totalNotes = notesData.length;
  const [notesCount, setNotesCount] = useState(notesPerPage);

  // Sort the notes by date:
  // - If the date is null we change it to 0 for ordering to work.
  const sortedNotes = notesData.sort((a, b) => {
    if ((a.date === null ? "0" : a.date) < (b.date === null ? "0" : b.date)) {
      return 1;
    } else {
      return -1;
    }
  });

  // Create the list notes according to the current notesCount:
  const notes = sortedNotes
    .slice(0, notesCount)
    .map((note) => <ReadingNote note={note} key={note.id} />);

  // If not all the notes are being shown, we show the button to load the next batch:
  // - The button updates the state notesCount and this automatically triggers a re-render, which will then show the updated notes list.
  var loadMoreButton;
  if (totalNotes > notesCount) {
    loadMoreButton = (
      <button
        onClick={() => setNotesCount(notesCount + notesPerPage)}
        id="load-more-buton"
      >
        &#8595; {t(translationsLibrary, locale, "load_more_button_label")}
      </button>
    );
  } else {
    loadMoreButton = "";
  }

  return (
    <>
      <NextSeo
        title={
          "fabricio c. boppré | " + t(translationsLibrary, locale, "title")
        }
        description={t(translationsLibrary, locale, "meta_description")}
        openGraph={{
          type: "website",
          url: process.env.basicURL + locale + pathname,
          images: [
            {
              url: process.env.basicURL + "images/open-graph-image.jpg",
              width: 1200,
              height: 630,
              alt: "fabricio c. boppré - web developer",
              type: "image/jpeg",
            },
          ],
        }}
        twitter={{
          cardType: "summary_large_image",
        }}
        additionalLinkTags={[
          {
            rel: "alternate",
            href: process.env.basicURL + "feed-anotacoes-de-leitura-" + locale + ".xml",
            type: "application/rss+xml",
            title: "fabricio c. boppré | " + t(translationsLibrary, locale, "title") + " | RSS feed"
          }
        ]}
      />

      <main id={styles.anotacoes_de_leitura} className="content">
        <article>
          <header>
            <h1>{t(translationsLibrary, locale, "title")}</h1>
            <a href={feedURL}
               className="rssIcon">
                <img src="/images/rss.svg" 
                     alt="RSS" />
            </a>
          </header>

          <ul>{notes}</ul>

          {loadMoreButton}
        </article>

        <BackToHomeButton />
      </main>
    </>
  );
}

AnotacoesDeLeitura.getLayout = function getLayout(page) {
  return <BasicLayout>{page}</BasicLayout>;
};

export async function getStaticProps(context) {
  // Generating the notes:
  const notesData = await getAnotacoesDeLeitura(context.locale);
  // Creating the RSS feed file:
  await generateRSS(context.locale, notesData);
  // Sending the notes to the component:
  return {
    props: {
      notesData,
    },
  };
}
