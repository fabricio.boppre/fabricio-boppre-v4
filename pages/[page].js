import BasicLayout from "components/BasicLayout";
import BackToHomeButton from "components/BackToHomeButton";
import { NextSeo } from "next-seo";
import { useRouter } from "next/router";
import { getPage } from "lib/pages";
import styles from "styles/Page.module.css";

export default function Page({ pageData }) {
  const router = useRouter();
  const { locale, asPath } = router;

  return (
    <>
      <NextSeo
        title={"fabricio c. boppré | " + pageData.title}
        description={pageData.meta_description}
        openGraph={{
          type: "website",
          url: process.env.basicURL + locale + asPath,
          images: [
            {
              url: process.env.basicURL + "images/open-graph-image.jpg",
              width: 1200,
              height: 630,
              alt: "fabricio c. boppré - web developer",
              type: "image/jpeg",
            },
          ],
        }}
        twitter={{
          cardType: "summary_large_image",
        }}
      />

      <main id={styles.page} className="content">
        <article>
          <h1>{pageData.title}</h1>
          <div dangerouslySetInnerHTML={{ __html: pageData.contentHtml }} />
        </article>

        <BackToHomeButton />
      </main>
    </>
  );
}

Page.getLayout = function getLayout(page) {
  return <BasicLayout>{page}</BasicLayout>;
};

export async function getStaticPaths() {
  return {
    paths: [
      { params: { page: "metodos_e_ferramentas" }, locale: "en" },
      { params: { page: "metodos_e_ferramentas" }, locale: "pt" },
      { params: { page: "leitura_web" }, locale: "en" },
      { params: { page: "leitura_web" }, locale: "pt" },
      { params: { page: "outros_interesses" }, locale: "en" },
      { params: { page: "outros_interesses" }, locale: "pt" },
      { params: { page: "perfis_e_redes_sociais" }, locale: "en" },
      { params: { page: "perfis_e_redes_sociais" }, locale: "pt" },
      { params: { page: "sobre_este_site" }, locale: "en" },
      { params: { page: "sobre_este_site" }, locale: "pt" },
    ],
    fallback: false,
  };
}

export async function getStaticProps(context) {
  const pageData = await getPage(context.params.page, context.locale, "index");
  return {
    props: {
      pageData,
    },
  };
}
