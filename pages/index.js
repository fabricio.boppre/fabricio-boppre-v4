import Link from "next/link";
import BasicLayout from "components/BasicLayout";
import { NextSeo } from "next-seo";
import { useRouter } from "next/router";
import { t } from "lib/localization/localization";
import translationsLibrary from "lib/localization/places/index.json";
import styles from "styles/Home.module.css";

export default function Home() {
  const router = useRouter();
  const { locale, pathname } = router;

  return (
    <>
      <NextSeo
        title="fabricio c. boppré"
        description={t(translationsLibrary, locale, "meta_description")}
        openGraph={{
          type: "website",
          url: process.env.basicURL + locale + pathname,
          images: [
            {
              url: process.env.basicURL + "images/open-graph-image.jpg",
              width: 1200,
              height: 630,
              alt: "fabricio c. boppré - web developer",
              type: "image/jpeg",
            },
          ],
        }}
        twitter={{
          cardType: "summary_large_image",
        }}
      />

      <main id={styles.home} className="content">
        <section id="intro">
          <h1>{t(translationsLibrary, locale, "intro_title")}</h1>
          <p>{t(translationsLibrary, locale, "intro_text")}</p>
        </section>
        <nav>
          <div id="lado_a">
            <h1>{t(translationsLibrary, locale, "lado_a_title")}</h1>
            <ul>
              <li>
                <Link href="/portfolio_websites">
                  <a>
                    {t(translationsLibrary, locale, "portfolio_link_title")}
                  </a>
                </Link>
              </li>
              <li className={locale !== "pt" ? "not-available" : null}>
                <a
                  href={"/files/curriculum_vitae.pdf"}
                  alt="curriculum vitæ"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  curriculum vitæ
                </a>
              </li>
              <li>
                <a
                  href="https://www.behance.net/fabricio-boppre"
                  target="_blank"
                  rel="noreferrer"
                >
                  {t(translationsLibrary, locale, "perfil_behance_link_title")}
                </a>
              </li>
              <li>
                <a
                  href={t(
                    translationsLibrary,
                    locale,
                    "perfil_linkedin_link_url"
                  )}
                  target="_blank"
                  rel="noreferrer"
                >
                  {t(translationsLibrary, locale, "perfil_linkedin_link_title")}
                </a>
              </li>
              <li>
                <Link href="/metodos_e_ferramentas">
                  <a>
                    {t(
                      translationsLibrary,
                      locale,
                      "metodos_ferramentas_link_title"
                    )}
                  </a>
                </Link>
              </li>
            </ul>
          </div>
          <div id="lado_b">
            <h1>{t(translationsLibrary, locale, "lado_b_title")}</h1>
            <ul>
              <li>
                <Link href="/anotacoes_de_leitura">
                  <a>
                    {t(
                      translationsLibrary,
                      locale,
                      "anotacoes_de_leitura_link_title"
                    )}
                  </a>
                </Link>
              </li>
              <li className={locale !== "pt" ? "not-available" : null}>
                <a
                  href="http://dyingdays.net/?author=fabricio_boppre"
                  target="_blank"
                  rel="noreferrer"
                >
                  {t(
                    translationsLibrary,
                    locale,
                    "escritos_sobre_musica_link_title"
                  )}
                </a>
              </li>
              <li>
                <Link href="/portfolio_ilustracoes">
                  <a>
                    {t(
                      translationsLibrary,
                      locale,
                      "portfolio_ilustracoes_link_title"
                    )}
                  </a>
                </Link>
              </li>
              <li>
                <Link href="/leitura_web">
                  <a>
                    {t(translationsLibrary, locale, "leitura_web_link_title")}
                  </a>
                </Link>
              </li>
              <li>
                <Link href="/outros_interesses">
                  <a>
                    {t(
                      translationsLibrary,
                      locale,
                      "outros_interesses_link_title"
                    )}
                  </a>
                </Link>
              </li>
            </ul>
          </div>
          <div id="estudos_experimentos_open-source">
            <h1>
              {t(translationsLibrary, locale, "studies_experiments_title")}
            </h1>
            <ul>
              <li>
                <a
                  href="https://gitlab.com/fabricio.boppre/tuimp"
                  target="_blank"
                  rel="noreferrer"
                >
                  tuimp
                </a>
              </li>
              <li>
                <a
                  href="https://bitbucket.org/fabricioboppre/humana/"
                  target="_blank"
                  rel="noreferrer"
                >
                  {t(translationsLibrary, locale, "humana_link_title")}
                </a>
              </li>
              <li>
                <a
                  href="https://bitbucket.org/fabricioboppre/natalia-vale-asari-v2/"
                  target="_blank"
                  rel="noreferrer"
                >
                  {t(
                    translationsLibrary,
                    locale,
                    "dra_natalia_vale_asari_link_title"
                  )}
                </a>
              </li>
              <li>
                <a
                  href="https://bitbucket.org/fabricioboppre/minha-colecao-de-discos/"
                  target="_blank"
                  rel="noreferrer"
                >
                  {t(
                    translationsLibrary,
                    locale,
                    "minha_colecao_de_discos_link_title"
                  )}
                </a>
              </li>
              <li>
                <a
                  href="https://gitlab.com/fabricio.boppre/filosofia"
                  target="_blank"
                  rel="noreferrer"
                >
                  {t(translationsLibrary, locale, "filosofia_link_title")}
                </a>
              </li>
            </ul>
          </div>
        </nav>
      </main>
    </>
  );
}

Home.getLayout = function getLayout(page) {
  return <BasicLayout>{page}</BasicLayout>;
};
