import Script from "next/script";
import IncompatibleBrowser from "components/IncompatibleBrowser";
import { Analytics } from "@vercel/analytics/react";
import "styles/globals.css";

// Custom App: https://nextjs.org/docs/advanced-features/custom-app
// - The `Component` prop is the active page, so whenever you navigate between routes, `Component` will change to the new page. Therefore, any props you send to `Component` will be received by the page;
// - `pageProps` is an object with the initial props that were preloaded for your page by one of our data fetching methods, otherwise it's an empty object.
function MyWebsite({ Component, pageProps }) {
  const getLayout = Component.getLayout || ((page) => page);

  return getLayout(
    <>
      <Component {...pageProps} />
      <IncompatibleBrowser />
      <Script src="/js/modernizr-custom.js" />
      <Analytics />
    </>
  );
}

export default MyWebsite;
