import BasicLayout from "components/BasicLayout";
import BackToHomeButton from "components/BackToHomeButton";
import { useEffect } from "react";
import { getSitesParaPortfolio } from "lib/portfolio_websites";
import { useRouter } from "next/router";
import { NextSeo } from "next-seo";
import { t } from "lib/localization/localization";
import translationsLibrary from "lib/localization/places/portfolio_websites.json";
import styles from "styles/PortfolioWebsites.module.css";

export default function PortfolioWebsites({ sitesData }) {
  const router = useRouter();
  const { locale, pathname } = router;

  // Sort the sites by date:
  const sortedSites = sitesData.sort((a, b) => {
    if (a.date < b.date) {
      return 1;
    } else {
      return -1;
    }
  });

  // Create the sites portfolio layout:
  const sites = sortedSites.map((site) => {
    var siteDiv;
    var image = (
      <img src={"/images/content/" + site.image} width="200" alt="" />
    );
    var title = (
      <h2>
        {site.title} ({site.date})
      </h2>
    );
    if (site.status == "online") {
      siteDiv = (
        <a key={site.id} className="siteThumbnail" href={site.url}>
          {image}
          <div className="about">
            {title}
            <div>{site.description}</div>
          </div>
        </a>
      );
    } else {
      siteDiv = (
        <div key={site.id} className="siteThumbnail">
          {image}
          <div className="about">
            {title}
            <div>{site.description}</div>
            <div>{site.status}</div>
          </div>
        </div>
      );
    }
    return siteDiv;
  });

  // This effect creates the portfolio grid with Masonry:
  // - https://www.npmjs.com/package/masonry-layout
  useEffect(() => {
    // Initialize Masonry:
    const Masonry = require("masonry-layout");
    var portfolio = document.querySelector("#thumbnails");
    var mosaico = new Masonry(portfolio, {
      columnWidth: 202, // 200px from the images plus their 1px border.
      gutter: 50, // Keep the same with "margin-bottom" value on styleshhet.
      itemSelector: ".siteThumbnail",
      fitWidth: true, // Works with "margin: 0 auto" on styleshhet.
    });
    // Trigger the layout method after images are loaded:
    // - https://www.npmjs.com/package/imagesloaded
    // - https://masonry.desandro.com/methods.html#layout-masonry
    const imagesLoaded = require("imagesloaded");
    imagesLoaded(portfolio).on("progress", function () {
      mosaico.layout();
    });
  }, []);

  return (
    <>
      <NextSeo
        title={
          "fabricio c. boppré | " + t(translationsLibrary, locale, "title")
        }
        description={t(translationsLibrary, locale, "meta_description")}
        openGraph={{
          type: "website",
          url: process.env.basicURL + locale + pathname,
          images: [
            {
              url: process.env.basicURL + "images/open-graph-image.jpg",
              width: 1200,
              height: 630,
              alt: "fabricio c. boppré - web developer",
              type: "image/jpeg",
            },
          ],
        }}
        twitter={{
          cardType: "summary_large_image",
        }}
      />

      <main id={styles.portfolio_websites} className="content">
        <article>
          <h1>{t(translationsLibrary, locale, "title")}</h1>

          <div id="thumbnails">{sites}</div>
        </article>

        <BackToHomeButton />
      </main>
    </>
  );
}

PortfolioWebsites.getLayout = function getLayout(page) {
  return <BasicLayout>{page}</BasicLayout>;
};

export async function getStaticProps(context) {
  const sitesData = await getSitesParaPortfolio(context.locale);
  return {
    props: {
      sitesData,
    },
  };
}
