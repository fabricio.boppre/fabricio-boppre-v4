import BasicLayout from "components/BasicLayout";
import BackToHomeButton from "components/BackToHomeButton";
import { useEffect } from "react";
import { getIlustracoesParaPortfolio } from "lib/portfolio_ilustracoes";
import { useRouter } from "next/router";
import { NextSeo } from "next-seo";
import { t } from "lib/localization/localization";
import translationsLibrary from "lib/localization/places/portfolio_ilustracoes.json";
import styles from "styles/PortfolioIlustracoes.module.css";

export default function PortfolioIlustracoes({ illustrationsData }) {
  const router = useRouter();
  const { locale, pathname } = router;

  // Sort the illustrations by date:
  const sortedIllustrations = illustrationsData.sort((a, b) => {
    if (a.date < b.date) {
      return 1;
    } else {
      return -1;
    }
  });

  // Create the illustrations portfolio layout:
  const illustrationsThumbnails = sortedIllustrations.map((illustration) => {
    return (
      <div
        key={illustration.id}
        id={illustration.id}
        onClick={() => showEnlargment(illustration.id + "-large")}
        className="illustrationThumbnail"
      >
        <img
          src={"/images/content/" + illustration.image_thumbnail}
          width="200"
          alt=""
        />
        <div className="about">
          <h2>
            {illustration.title} ({illustration.date})
          </h2>
          <div>{illustration.description}</div>
        </div>
      </div>
    );
  });

  // Create the illustrations enlargments:
  const illustrationsEnlargments = sortedIllustrations.map((illustration) => {
    return (
      <div
        key={illustration.id}
        id={illustration.id + "-large"}
        className="illustrationEnlargment"
      >
        <img src={"/images/content/" + illustration.image_large} alt="" />
        <nav>
          <button
            className="previous"
            onClick={() => showEnlargmentPrevious(illustration.id + "-large")}
          >
            {t(translationsLibrary, locale, "button_previous")}
          </button>
          <button onClick={() => closeEnlargment(illustration.id + "-large")}>
            {t(translationsLibrary, locale, "button_close")}
          </button>
          <button
            className="next"
            onClick={() => showEnlargmentNext(illustration.id + "-large")}
          >
            {t(translationsLibrary, locale, "button_next")}
          </button>
        </nav>
      </div>
    );
  });

  // Function to show the enlarged illustration:
  function showEnlargment(imageID) {
    document.body.style.overflow = "hidden";
    document.getElementById(imageID).classList.add("current");
  }

  // Function to close the enlarged illustration:
  function closeEnlargment(imageID) {
    document.body.style.overflow = "auto";
    document.getElementById(imageID).classList.remove("current");
  }

  // Function to show the next enlarged illustration:
  function showEnlargmentNext(imageID) {
    document.getElementById(imageID).classList.remove("current");
    document.getElementById(imageID).nextSibling.classList.add("current");
  }

  // Function to show the previous enlarged illustration:
  function showEnlargmentPrevious(imageID) {
    document.getElementById(imageID).classList.remove("current");
    document.getElementById(imageID).previousSibling.classList.add("current");
  }

  // This effect creates the portfolio grid with Masonry:
  // - https://www.npmjs.com/package/masonry-layout
  useEffect(() => {
    // Initialize Masonry:
    const Masonry = require("masonry-layout");
    var portfolio = document.querySelector("#thumbnails");
    var mosaico = new Masonry(portfolio, {
      columnWidth: 202, // 200px from the images plus their 1px border.
      gutter: 50, // Keep the same with "margin-bottom" value on styleshhet.
      horizontalOrder: true, // Keep the layout order (needed to allow the nav buttons to work properly).
      itemSelector: ".illustrationThumbnail",
      fitWidth: true, // Works with "margin: 0 auto" on styleshhet.
    });
    // Trigger the layout method after images are loaded:
    // - https://www.npmjs.com/package/imagesloaded
    // - https://masonry.desandro.com/methods.html#layout-masonry
    const imagesLoaded = require("imagesloaded");
    imagesLoaded(portfolio).on("progress", function () {
      mosaico.layout();
    });
  }, []);

  return (
    <>
      <NextSeo
        title={
          "fabricio c. boppré | " + t(translationsLibrary, locale, "title")
        }
        description={t(translationsLibrary, locale, "meta_description")}
        openGraph={{
          type: "website",
          url: process.env.basicURL + locale + pathname,
          images: [
            {
              url: process.env.basicURL + "images/open-graph-image.jpg",
              width: 1200,
              height: 630,
              alt: "fabricio c. boppré - web developer",
              type: "image/jpeg",
            },
          ],
        }}
        twitter={{
          cardType: "summary_large_image",
        }}
      />

      <main id={styles.portfolio_illustrations} className="content">
        <div id="enlargments">{illustrationsEnlargments}</div>

        <article>
          <h1>{t(translationsLibrary, locale, "title")}</h1>

          <div id="thumbnails">{illustrationsThumbnails}</div>
        </article>

        <BackToHomeButton />
      </main>
    </>
  );
}

PortfolioIlustracoes.getLayout = function getLayout(page) {
  return <BasicLayout>{page}</BasicLayout>;
};

export async function getStaticProps(context) {
  const illustrationsData = await getIlustracoesParaPortfolio(context.locale);
  return {
    props: {
      illustrationsData,
    },
  };
}
