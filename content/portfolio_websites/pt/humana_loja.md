---
title: humana - loja virtual
date: 2021
description: next.js, react, sanity, snipcart, stripe, aws lambda, algolia, css3, html5 & sass
status: online
url: http://loja.humanasebolivraria.com.br/
image: portfolio-site-humana-loja.jpg
---
