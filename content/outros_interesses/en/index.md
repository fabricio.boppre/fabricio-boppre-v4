---
title: "other interests"
meta_description: "Some other things that interest me."
---

- **[pynchonWiki](http://pynchonwiki.com)**: #Literature #ThomasPynchon

- **[The Cormac McCarthy Society](https://www.cormacmccarthy.com)**: #Literature #CormacMcCarthy

- **[The Living Archive of Underground Music](http://livingarchive.doncampau.com/about/a-brief-history-of-cassette-culture)**: #Music #Underground #CassetteTapes

- **[tapedeck.org](http://tapedeck.org)**: #Music #Nostalgia #CassetteTapes

- **[Classical Net](http://www.classical.net/index.php)**: #Music #ClassicalMusic

- **[ECM Records](https://www.ecmrecords.com/home)**: #Music #Jazz #ClassicalMusic #ECMRecords

- **[John Coltrane.com](http://www.johncoltrane.com)**: #Music #JohnColtrane

- **[Midnight Oil](https://www.midnightoil.com)**: #Music #MidnightOil

- **[The Walt Whitman Archive](http://whitmanarchive.org)**: #Poetry #WaltWhitman

- **[nostalghia.com](http://nostalghia.com)**: #Movies #AndreiTarkovsky

- **[Ludwig van Beethoven website](http://www.lvbeethoven.com)**: #Music #LudwigVanBeethoven

- **[Internet Encyclopedia of Philosophy](http://www.iep.utm.edu)**: #Philosophy

- **[The Kafka Project](http://www.kafka.org/index.php)**: #Literature #FranzKafka

- **[RSPB | Birds & wildlife](https://www.rspb.org.uk/birds-and-wildlife/wildlife-guides/)**: #Nature #BirdIdentification

- **[Indiana Jones and the Fate of Atlantis](http://indianajones.wikia.com/wiki/Indiana_Jones_and_the_Fate_of_Atlantis)**: #VideoGame #IndianaJonesAndTheFateOfAtlantis

- **[L.A. Noire Wiki](http://lanoire.wikia.com/wiki/L.A._Noire_Wiki)**: #VideoGame #LANoire

- **[Old Book Illustrations](https://www.oldbookillustrations.com)**: #Literature #Illustration #OldBooks

- **[chessgames.com](http://www.chessgames.com)**: #Chess

- **[Half-Life Wiki](http://half-life.wikia.com/)**: #VideoGame #HalfLife

- **[Encyclopaedia Metallum](https://www.metal-archives.com)**: #Music #HeavyMetal

- **[Dharma Beat](http://www.dharmabeat.com)**: #Literature #JackKerouac

- **[Adventure Gamers](https://adventuregamers.com)**: #VideoGame #Adventures

- **[Conan Wiki](http://conan.wikia.com/)**: #Comics #Conan

- **[Southern Lord](https://www.southernlord.com)**: #Music #HeavyMetal #SouthernLord

- **[Surfrider Foundation](http://www.surfrider.org)**: #Surf #Nature

- **[Surfer Magazine](https://www.surfer.com)**: #Surf

- **[openMSX](http://openmsx.org)**: #ComputerScience #VideoGame #Nostalgia #MSX

- **[Raspberry Pi](https://www.raspberrypi.com/)**: #ComputerScience #RaspberryPi

- **[Red Dead Wiki](http://reddead.wikia.com/)**: #VideoGame #RedDeadRedemption

- **[AtariAge](http://www.atariage.com/?)**: #VideoGame #ostalgia #Atari

- **[The Arthur Conan Doyle Encyclopedia](https://www.arthur-conan-doyle.com/index.php?title=A_Study_in_Sherlock)**: #Literature #ArthurConanDoyle #SherlockHolmes

- **[N.D.Pântano do Sul](http://pantanodosul.blogspot.com)**: #Florianópolis #Nature

- **[Peanuts](http://www.gocomics.com/peanuts)**: #Comics #Peanuts

- **[Calvin and Hobbes](http://www.gocomics.com/calvinandhobbes/)**: #Comics #CalvinAndHobbes

- **[The Iron Maiden Commentary](http://www.ironmaidencommentary.com)**: #Music #IronMaiden

- **[John Carpenter](https://theofficialjohncarpenter.com)**: #Movies #Music #JohnCarpenter

- **[The Tolkien Society](https://www.tolkiensociety.org)**: #Literature #JRRTolkien

- **[Johannes Brahms](http://www.classical-music.com/article/who-was-johannes-brahms)**: #Music #JohannesBrahms

- **[Kubrick 2001](http://www.kubrick2001.com)**: #Movies #2001ASpaceOdyssey

- **[Stanley Kubrick](http://www.stanleykubrick.de/en/)**: #Movies #StanleyKubrick

- **[Hammer Films](http://www.hammerfilms.com)**: #Movies #Horror #Dracula #HammerFilms

- **[Gabriel Knight Omnipedia](http://gabrielknight.wikia.com/)**: #VideoGame #GabrielKnight

- **[The Vincent Price Exhibit](http://vincentpriceexhibit.com)**: #Movies #Horror #VincentPrice

- **[Bela Lugosi](http://belalugosi.com)**: #Movies #Horror #BelaLugosi

- **[Red Dead Wiki](https://reddead.fandom.com/wiki/Red_Dead_Wiki)**: #VideoGame #RedDeadRedemption

- **[Snow in Berlin](http://www.snowinberlin.com/index.html)**: #Music #TalkTalk
