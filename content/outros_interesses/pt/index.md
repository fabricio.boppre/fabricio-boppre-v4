---
title: "outros interesses"
meta_description: "Algumas outras coisas que me interessam."
---

- **[pynchonWiki](http://pynchonwiki.com)**: #Literatura #ThomasPynchon

- **[The Cormac McCarthy Society](https://www.cormacmccarthy.com)**: #Literatura #CormacMcCarthy

- **[The Living Archive of Underground Music](http://livingarchive.doncampau.com/about/a-brief-history-of-cassette-culture)**: #Música #Underground #FitasCassetes

- **[tapedeck.org](http://tapedeck.org)**: #Música #Nostalgia #FitasCassetes

- **[Classical Net](http://www.classical.net/index.php)**: #Música #MúsicaClássica

- **[ECM Records](https://www.ecmrecords.com/home)**: #Música #Jazz #MúsicaClássica #ECMRecords

- **[John Coltrane.com](http://www.johncoltrane.com)**: #Música #JohnColtrane

- **[Midnight Oil](https://www.midnightoil.com)**: #Música #MidnightOil

- **[The Walt Whitman Archive](http://whitmanarchive.org)**: #Poesia #WaltWhitman

- **[nostalghia.com](http://nostalghia.com)**: #Cinema #AndreiTarkovsky

- **[Ludwig van Beethoven website](http://www.lvbeethoven.com)**: #Música #LudwigVanBeethoven

- **[Internet Encyclopedia of Philosophy](http://www.iep.utm.edu)**: #Filosofia

- **[The Kafka Project](http://www.kafka.org/index.php)**: #Literatura #FranzKafka

- **[RSPB | Birds & wildlife](https://www.rspb.org.uk/birds-and-wildlife/wildlife-guides/)**: #Natureza #IdentificaçãoDePássaros

- **[Indiana Jones and the Fate of Atlantis](http://indianajones.wikia.com/wiki/Indiana_Jones_and_the_Fate_of_Atlantis)**: #VideoGame #IndianaJonesAndTheFateOfAtlantis

- **[L.A. Noire Wiki](http://lanoire.wikia.com/wiki/L.A._Noire_Wiki)**: #VideoGame #LANoire

- **[Old Book Illustrations](https://www.oldbookillustrations.com)**: #Literatura #Ilustração #LivrosAntigos

- **[chessgames.com](http://www.chessgames.com)**: #Xadrez

- **[Half-Life Wiki](http://half-life.wikia.com/)**: #VideoGame #HalfLife

- **[Encyclopaedia Metallum](https://www.metal-archives.com)**: #Música #HeavyMetal

- **[Dharma Beat](http://www.dharmabeat.com)**: #Literatura #JackKerouac

- **[Adventure Gamers](https://adventuregamers.com)**: #VideoGame #Adventures

- **[Conan Wiki](http://conan.wikia.com/)**: #Quadrinhos #Conan

- **[Southern Lord](https://www.southernlord.com)**: #Música #HeavyMetal #SouthernLord

- **[Surfrider Foundation](http://www.surfrider.org)**: #Surf #Natureza

- **[Surfer Magazine](https://www.surfer.com)**: #Surf

- **[openMSX](http://openmsx.org)**: #Computação #VideoGame #Nostalgia #MSX

- **[Raspberry Pi](https://www.raspberrypi.com/)**: #Computação #RaspberryPi

- **[Red Dead Wiki](http://reddead.wikia.com/)**: #VideoGame #RedDeadRedemption

- **[AtariAge](http://www.atariage.com/?)**: #VideoGame #Nostalgia #Atari

- **[The Arthur Conan Doyle Encyclopedia](https://www.arthur-conan-doyle.com/index.php?title=A_Study_in_Sherlock)**: #Literatura #ArthurConanDoyle #SherlockHolmes

- **[N.D.Pântano do Sul](http://pantanodosul.blogspot.com)**: #Florianópolis #Natureza

- **[Peanuts](http://www.gocomics.com/peanuts)**: #Quadrinhos #Peanuts

- **[Calvin and Hobbes](http://www.gocomics.com/calvinandhobbes/)**: #Quadrinhos #CalvinAndHobbes

- **[The Iron Maiden Commentary](http://www.ironmaidencommentary.com)**: #Música IronMaiden

- **[John Carpenter](https://theofficialjohncarpenter.com)**: #Cinema #Música #JohnCarpenter

- **[The Tolkien Society](https://www.tolkiensociety.org)**: #Literatura #JRRTolkien

- **[Johannes Brahms](http://www.classical-music.com/article/who-was-johannes-brahms)**: #Música #JohannesBrahms

- **[Kubrick 2001](http://www.kubrick2001.com)**: #Cinema #2001ASpaceOdyssey

- **[Stanley Kubrick](http://www.stanleykubrick.de/en/)**: #Cinema #StanleyKubrick

- **[Hammer Films](http://www.hammerfilms.com)**: #Cinema #Horror #Drácula #HammerFilms

- **[Gabriel Knight Omnipedia](http://gabrielknight.wikia.com/)**: #VideoGame #GabrielKnight

- **[The Vincent Price Exhibit](http://vincentpriceexhibit.com)**: #Cinema #Horror #VincentPrice

- **[Bela Lugosi](http://belalugosi.com)**: #Cinema #Horror #BelaLugosi

- **[Red Dead Wiki](https://reddead.fandom.com/wiki/Red_Dead_Wiki)**: #VideoGame #RedDeadRedemption

- **[Snow in Berlin](http://www.snowinberlin.com/index.html)**: #Música #TalkTalk
