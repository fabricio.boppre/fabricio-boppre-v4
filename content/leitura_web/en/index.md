---
title: "what I read on the web"
meta_description: "Websites on various subjects that I like to read."
---

- **[Infinite Improbabilty Drive](https://infiniteimprobabiltydrive.tumblr.com)**: Poems of an astronomer.

- **[Wire](https://www.thewire.co.uk)**: Website about contemporary music.

- **[Arts & Letters Daily](https://www.aldaily.com)**: Texts (mostly) about writers, philosophy and literature.

- **[ClassicsToday.com](https://www.classicstoday.com)**: News and reviews about classical music.

- **[Musicophilesblog](https://musicophilesblog.com)**: Blog about classical and jazz music — from "Keith Jarrett to Johannes Brahms", as its author puts it.

- **[between sound and space](https://ecmreviews.com)**: Blog with album reviews, especially those released by the German label ECM.

- **[Nexo](https://www.nexojornal.com.br)**: Digital newspaper with news about Brazil and the world.

- **[The Intercept Brasil](https://theintercept.com/brasil/)**: Investigative website with a focus on politics.

- **[No Clean Singing](http://www.nocleansinging.com)**: Blog about heavy metal and surroundings.

- **[Gramophone](https://www.gramophone.co.uk/)**: Website of Gramophone, an English magazine dedicated to classical music.

- **[Bloody Disgusting](http://bloody-disgusting.com)**: Website about horror movies and video games.

- **[Bloodvine](https://bloodvine.com)**: Website about horror movies.

- **[Pluralistic: Daily links from Cory Doctorow](https://pluralistic.net)**: Website by writer and political, cultural and technology commentator Cory Doctorow.

- **[piauí](http://piaui.folha.uol.com.br)**: Website of piauí, a Brazilian magazine.

- **[Surfline](https://www.surfline.com)**: Surfing news and wave forecast.

- **[Charlles Campos](http://charllescampos.blogspot.com.br)**: My friend Charlles Campos's blog.

- **[CVLT Nation](http://www.cvltnation.com)**: Website about underground art, focusing on music, video and visual arts.

- **[The Quietus](http://thequietus.com)**: My favorite website about music. Indispensable daily reading.

- **[Alex Ross: The Rest Is Noise](http://www.therestisnoise.com)**: American music critic Alex Ross website.

- **[GameSpot](https://www.gamespot.com)**: Website about video games.

- **[Angry Metal Guy](https://www.angrymetalguy.com)**: Heavy metal music blog.

- **[Destructoid](https://www.destructoid.com)**: Website about video games.

- **[The New Yorker](http://www.newyorker.com)**: Website of the American magazine The New Yorker.

- **[Dveras em Rede](http://www.dauroveras.com.br)**: My friend Dauro Veras' blog.

- **[All About Birds](https://www.allaboutbirds.org)**: Website about birdwatching.

- **[National Geographic](https://www.nationalgeographic.com)**: National Geographic website.

- **[Wired](https://www.wired.com)**: Website about science and technology.

- **[Slashdot](https://slashdot.org)**: Website about technology.

- **[This Won't be for Everyone](https://burrellosubmarinemovies.wordpress.com)**: A blog about movies.

- **[So Few Critics, So Many Poets](https://scottross79.wordpress.com)**: This is also a blog about movies, but here the reviews are long and detailed.

- **[Space: 1970](http://space1970.blogspot.com.br)**: Website about science fiction movies, especially old or B movies (usually both at the same time).

- **[My First Trip Abroad](https://vincentpricejournal.wordpress.com)**: Transcript of the diary that the great actor and renaissance man Vincent Price kept during his first trip outside the US.

- **[Euterpe](http://euterpe.blog.br)**: Blog about classical music.

- **[The Guardian](https://www.theguardian.com/international)**: The best European newspaper. Indispensable daily reading.

- **[Surfguru](https://www.surfguru.com.br)**: Surfing news and wave forecast.

- **[DigitalArts](http://www.digitalartsonline.co.uk)**: Website about digital art.

- **[Blog do Brüggemann](http://bloguedobruggemann.blogspot.com.br)**: Blog by writer Fábio Brüggemann.

- **[Meio](https://www.canalmeio.com.br/ultima-edicao/)**: Collected daily news about Brazil and the world.

- **[the inertia](https://www.theinertia.com)**: Website about surfing, environment and outdoor life.

- **[Surf & Cult](http://www.surfecult.com)**: Website about surfing and surfing culture.

- **[The Sound of Vincent Price](https://www.thesoundofvincentprice.com)**: The title of this website says it all.

- **[Astronomy Picture of the Day](https://apod.nasa.gov/apod/astropix.html)**: Images and their respective descriptions of the cosmos. Indispensable daily reading.

- **[From the Dust Returned](http://autothrall.blogspot.com/)**: Heavy metal reviews.

- **[Notebook](https://mubi.com/notebook)**: Great blog about movies written by the good people of [Mubi](https://mubi.com/showing).

- **[Bandcamp Daily](https://daily.bandcamp.com)**: Great blog about music by the [bandcamp](https://bandcamp.com) writers staff.

- **[Ivan Jerônimo](https://ivanjeronimo.com.br)**: Ivan Jerônimo's blog. Ivan is a great illustrator and a friend of mine.

- **[Manual do Usuário](https://manualdousuario.net/)**: Great blog about technology. It doesn't run from the political and social implications of the internet, social networks, etc.

- **[Farfalhada](https://buttondown.com/farfalhada/archive)**: Newsletter from friend and book editor Felipe Moreno.

- **[Mike Monteiro’s Good News](https://buttondown.com/monteiro/archive)**: I don't know who exactly Mike Monteiro is but his newsletter is great.
