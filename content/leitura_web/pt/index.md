---
title: "o que eu leio na web"
meta_description: "Websites sobre assuntos diversos que gosto de ler."
---

- **[Infinite Improbabilty Drive](https://infiniteimprobabiltydrive.tumblr.com)**: Poemas de uma astrônoma.

- **[Wire](https://www.thewire.co.uk)**: Site sobre música contemporânea.

- **[Arts & Letters Daily](https://www.aldaily.com)**: Textos sobre (em sua grande maioria) escritores, filosofia e literatura.

- **[ClassicsToday.com](https://www.classicstoday.com)**: Site com resenhas e notícias sobre música clássica.

- **[Musicophilesblog](https://musicophilesblog.com)**: Blog sobre música clássica e jazz — de "Keith Jarrett a Johannes Brahms", como diz seu autor.

- **[between sound and space](https://ecmreviews.com)**: Blog com resenhas de discos, sobretudo aqueles lançados pelo selo alemão ECM.

- **[Nexo](https://www.nexojornal.com.br)**: Jornal digital com notícias sobre o Brasil e o mundo.

- **[The Intercept Brasil](https://theintercept.com/brasil/)**: Site investigativo com foco em política.

- **[No Clean Singing](http://www.nocleansinging.com)**: Blog sobre heavy metal e adjacências.

- **[Gramophone](https://www.gramophone.co.uk/)**: Site da Gramophone, revista inglesa dedicada à música clássica.

- **[Bloody Disgusting](http://bloody-disgusting.com)**: Site sobre filmes e games de horror.

- **[Bloodvine](https://bloodvine.com)**: Site sobre filmes de horror.

- **[Pluralistic: Daily links from Cory Doctorow](https://pluralistic.net)**: Site do escritor e comentarista de política, cultural e tecnologia Cory Doctorow.

- **[piauí](http://piaui.folha.uol.com.br)**: Site da revista piauí.

- **[Surfline](https://www.surfline.com)**: Notícias sobre o mundo do surf e previsão de ondas.

- **[Charlles Campos](http://charllescampos.blogspot.com.br)**: Blog do amigo Charlles Campos.

- **[CVLT Nation](http://www.cvltnation.com)**: Site sobre arte underground, com foco em música, vídeo e artes plásticas.

- **[The Quietus](http://thequietus.com)**: Meu site sobre música favorito. Leitura diária indispensável.

- **[Alex Ross: The Rest Is Noise](http://www.therestisnoise.com)**: Site do crítico de música americano Alex Ross.

- **[GameSpot](https://www.gamespot.com)**: Site sobre games.

- **[Angry Metal Guy](https://www.angrymetalguy.com)**: Blog sobre heavy metal.

- **[Destructoid](https://www.destructoid.com)**: Site sobre games.

- **[The New Yorker](http://www.newyorker.com)**: Site da revista americana The New Yorker.

- **[Dveras em Rede](http://www.dauroveras.com.br)**: Blog do amigo Dauro Veras.

- **[All About Birds](https://www.allaboutbirds.org)**: Site sobre observação de pássaros.

- **[National Geographic](https://www.nationalgeographic.com)**: Site da revista National Geographic.

- **[Wired](https://www.wired.com)**: Site sobre ciência e tecnologia.

- **[Slashdot](https://slashdot.org)**: Site sobre tecnologia.

- **[This Won't be for Everyone](https://burrellosubmarinemovies.wordpress.com)**: Blog sobre filmes.

- **[So Few Critics, So Many Poets](https://scottross79.wordpress.com)**: Também este é um blog sobre filmes, porém aqui as críticas são longas e detalhadas.

- **[Space: 1970](http://space1970.blogspot.com.br)**: Site sobre cinema de ficção científica, sobretudo filmes antigos ou filmes B (em geral, os dois ao mesmo tempo).

- **[My First Trip Abroad](https://vincentpricejournal.wordpress.com)**: Transcrição do diário que o grande ator e _renaissance man_ Vincent Price manteve durante sua primeira viagem fora dos EUA.

- **[Euterpe](http://euterpe.blog.br)**: Blog sobre música clássica.

- **[The Guardian](https://www.theguardian.com/international)**: O melhor jornal europeu. Leitura diária indispensável.

- **[Surfguru](https://www.surfguru.com.br)**: Previsão de ondas e notícias sobre surf.

- **[DigitalArts](http://www.digitalartsonline.co.uk)**: Site sobre artes digitais.

- **[Blog do Brüggemann](http://bloguedobruggemann.blogspot.com.br)**: Blog do escritor Fábio Brüggemann.

- **[Meio](https://www.canalmeio.com.br/ultima-edicao/)**: Apanhado diário de notícias pelo Brasil e pelo mundo.

- **[the inertia](https://www.theinertia.com)**: Site sobre surf, meio ambiente e vida ao ar livre.

- **[Surf & Cult](http://www.surfecult.com)**: Site sobre surf e cultura surf.

- **[The Sound of Vincent Price](https://www.thesoundofvincentprice.com)**: O título deste site diz tudo.

- **[Astronomy Picture of the Day](https://apod.nasa.gov/apod/astropix.html)**: Imagens e suas respectivas descrições sobre o cosmos. Leitura diária indispensável.

- **[From the Dust Returned](http://autothrall.blogspot.com/)**: Resenhas de discos de heavy metal.

- **[Notebook](https://mubi.com/notebook)**: Ótimo blog sobre cinema mantido pelo [Mubi](https://mubi.com/showing).

- **[Bandcamp Daily](https://daily.bandcamp.com)**: Blog sobre música mantido pelo [bandcamp](https://bandcamp.com).

- **[Ivan Jerônimo](https://ivanjeronimo.com.br)**: Blog do amigo e ilustrador Ivan Jerônimo.

- **[Manual do Usuário](https://manualdousuario.net/)**: Ótimo blog sobre tecnologia. Não foge das implicações político e sociais da internet, redes sociais, etc.

- **[Farfalhada](https://buttondown.com/farfalhada/archive)**: Newsletter do amigo e editor de livros artesanais Felipe Moreno.

- **[Mike Monteiro’s Good News](https://buttondown.com/monteiro/archive)**: Eu não sei quem examente é Mike Monteiro, mas sua newsletter é muito boa.
