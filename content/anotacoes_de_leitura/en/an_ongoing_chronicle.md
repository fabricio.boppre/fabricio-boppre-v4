---
title: An ongoing chronicle
date: 2024-11-21T10:46:11.148Z
credit: Matthew Ismael Ruiz about digital music collections (copied from
  [here](https://pitchfork.com/features/article/the-obsessive-world-of-digital-music-collectors/))
---
My digital music collection is more than just a cache of songs, it’s a record of my life: mixes made for and by lovers, early demos from defunct bands first seen in local VFW halls, original versions of classic records that are long out of print. Through the years, I’ve meticulously ripped, downloaded, and tagged these records, before backing them up to multiple hard drives and migrating them from computer to computer. They’re part of a personal library I’ve been maintaining since I downloaded my first MP3s from private AOL chat rooms circa 1996—an ongoing chronicle of my musical history and education that nonetheless remains somewhat intangible, existing only as a series of ones and zeroes carved into the platters of spinning hard disks.