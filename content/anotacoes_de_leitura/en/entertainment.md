---
title: Entertainment
credit: Kurt Vonnegut
---

One of the few good things about modern life: if you die horribly on television, you will not have died in vain. You will have entertained us.
