---
title: Decent people
date: 2019-08-23
credit: Jill Filipovic in [*A new poll shows what really interests 'pro-lifers'&#58; controlling women*](https://www.theguardian.com/commentisfree/2019/aug/22/a-new-poll-shows-what-really-interests-pro-lifers-controlling-women) (*The Guardian*, August 2019)
---

In the aftermath of the 2016 election, mostly white pundits wondered if Donald Trump’s white male base was motivated by “economic anxiety”. We heard this over and over: Trump voters aren’t the racist deplorables the liberal media (of which those same pundits were a part) makes them out to be. They’re decent people who have been hurt by free trade agreements, increasing Chinese economic dominance, the decimation of unions, a thinning social safety net, and stagnating wages. (Why those same people would then turn around and vote for a party that kills unions, tears up the safety net and blocks minimum wage raises while cutting taxes for CEOs went unexplained.) Then came the social scientists – and whaddaya know? Trump voters weren’t motivated by economic anxiety as much as fear of “cultural displacement”. White Christian men (and many of their wives) were so used to their cultural, political and economic dominance that they perceived the ascension of other groups as a threat.
