---
title: Less than empty words
date: 2022-06-26T13:48:27.850Z
credit: Mara van der Lugt in [*Look on the dark
  side*](https://aeon.co/essays/in-these-dark-times-the-virtue-we-need-is-hopeful-pessimism)
  ([*Aeon*](https://aeon.co), 26 April 2022)
---

Any crude statements of optimism would be more than misplaced: it would be the kind of lie that deceives no one, least of all the sharpened moral senses of the young, who see through the empty promises and reassurances of politicians with an anger we know is justified. If we told them that everything will be OK, these are less than empty words: they are a failure to take their experience seriously, and that, as the pessimists would tell us, is the one thing guaranteed to make their suffering worse.
