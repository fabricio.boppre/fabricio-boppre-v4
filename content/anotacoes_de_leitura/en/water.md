---
title: Water
credit: Loren Eiseley
---

If there is magic on this planet, it is contained in water.
