---
title: "Our hunting fathers"
date: 2020-09-03
credit: "W. H. Auden"
---

Our hunting fathers told the story  
Of the sadness of the creatures,  
Pitied the limits and the lack  
Set in their finished features;  
Saw in the lion's intolerant look,  
Behind the quarry's dying glare,  
Love raging for, the personal glory  
That reason's gift would add,  
The liberal appetite and power,  
The rightness of a god.

Who, nurtured in that fine tradition,  
Predicted the result,  
Guessed Love by nature suited to  
The intricate ways of guilt,  
That human ligaments could so  
His southern gestures modify  
And make it his mature ambition  
To think no thought but ours,  
To hunger, work illegally,  
And be anonymous?
