---
title: Pep Talk
credit: Ron Padgett
---

Dinner is a damned nice thing  
as are breakfast and lunch  
when they’re good and with  
the one you love.  
That’s a kind of dancing  
sitting down and not moving  
but what dances exactly  
we do not know nor  
need to know,  
it is dancing us around  
and nothing is moving  
in the miracle of dinner  
breakfast and lunch  
and all the in-betweens  
that give us pep.
