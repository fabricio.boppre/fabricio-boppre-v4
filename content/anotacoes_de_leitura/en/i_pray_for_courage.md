---
title: I Pray for Courage
date: 2018-12-10
credit: Leonard Cohen
---

I pray for courage  
Now I’m old  
To greet the sickness  
And the cold

I pray for courage  
In the night  
To bear the burden  
Make it light

I pray for courage  
In the time  
When suffering comes and  
Starts to climb

I pray for courage  
At the end  
To see death coming  
As a friend
