---
title: Society
date: 2022-03-25
credit: Oscar Wilde in *De Profundis*
---

Society takes upon itself the right to inflict appalling punishments on the individual, but it also has the supreme vice of shallowness, and fails to realise what it has done. When the man’s punishment is over, it leaves him to himself: that is to say it abandons him at the very moment when its highest duty towards him begins. It is really ashamed of its own actions, and shuns those whom it has punished, as people shun a creditor whose debt they cannot pay, or one on whom they have inflicted an irreparable, an irredeemable wrong. I claim on my side that if I realise what I have suffered, Society should realise what it has inflicted on me: and there should be no bitterness or hate on either side.
