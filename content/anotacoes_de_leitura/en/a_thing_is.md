---
title: A thing is
date: 2022-04-17T11:30:49.288Z
credit: Oscar Wilde in *De Profundis*
---

For yourself, I have but this last thing to say. Do not be afraid of the past. If people tell you that it is irrevocable, do not believe them. The past, the present and the future are but one moment in the sight of God, in whose sight we should try to live. Time and space, succession and extension, are merely accidental conditions of Thought. The Imagination can transcend them, and move in a free sphere of ideal existences. Things, also, are in their essence what we choose to make them. A thing is, according to the mode in which one looks at it. ‘Where others’, says Blake, ‘see but the Dawn coming over the hill, I see the sons of God shouting for joy.’
