---
title: Just as the ocean has waves
date: 2019-12-16
credit: Timothy Morton in *Being Ecological*
---

The way attunement is deep rather than superficial is why the legend has it that Buddha taught meditation as a form of tuning: just as a sitar string should be neither too tight nor too loose, so one’s mindful focus on the meditation object — a mantra, your breathing, whatever single object is the focus of your meditation — should be alert but relaxed. The conversation between “alert” and “relaxed” forms a dynamic system that simply can’t remain still: hence the phenomenon that many beginning meditators experience, that their thoughts are rushing, because they are simply observing the intrinsic, rather than superficial, qualities of mind as such — mind thinks (in the largest sense of that word), mind “minds”, just as the ocean has waves. Movement is intrinsic. This fact becomes especially interesting when the meditation object is mind as such: when mind tunes to mind. What is experienced here is not absolutely nothing, but rather a strange beingness that cannot be pinned down to a presence I can point at. There is a very deep ontological reason for this: appearing (waves) is intrinsic to being (ocean), yet different.
