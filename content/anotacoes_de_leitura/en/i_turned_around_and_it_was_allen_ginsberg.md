---
title: I turned around and it was Allen Ginsberg
date: 2023-11-19T17:04:50.680Z
credit: Patti Smith in *Just Kids*
---
I was always hungry. I metabolized my food quickly. Robert could go without eating much longer than me. If we were out of money we just didn’t eat. Robert might be able to function, even if he got a little shaky, but I would feel like I was going to pass out. One drizzly afternoon I had a hankering for one of those cheese-and-lettuce sandwiches. I went through our belongings and found exactly fifty-five cents, slipped on my gray trench coat and Mayakovsky cap, and headed to the Automat.

I got my tray and slipped in my coins but the window wouldn’t open. I tried again without luck and then I noticed the price had gone up to sixty-five cents. I was disappointed, to say the least, when I heard a voice say, “Can I help?”

I turned around and it was Allen Ginsberg. We had never met but there was no mistaking the face of one of our great poets and activists. I looked into those intense dark eyes punctuated by his dark curly beard and just nodded. Allen added the extra dime and also stood me to a cup of coffee. I wordlessly followed him to his table, and then plowed into the sandwich.

Allen introduced himself. He was talking about Walt Whitman and I mentioned I was raised near Camden, where Whitman was buried, when he leaned forward and looked at me intently. “Are you a girl?” he asked.

“Yeah,” I said. “Is that a problem?”

He just laughed. “I’m sorry. I took you for a very pretty boy.”

I got the picture immediately.

“Well, does this mean I return the sandwich?”

“No, enjoy it. It was my mistake.”

He told me he was writing a long elegy for Jack Kerouac, who had recently passed away. “Three days after Rimbaud’s birthday,” I said. I shook his hand and we parted company.

Sometime later Allen became my good friend and teacher. We often reminisced about our first encounter and he once asked how I would describe how we met. “I would say you fed me when I was hungry,” I told him. And he did.