---
title: "Old maps"
date: 2021-04-16
credit: "Cormac McCarthy in *Blood Meridian*"
---

They cut the throats of the packanimals and jerked and divided the meat and they traveled under the cape of the wild mountains upon a broad soda plain with dry thunder to the south and rumors of light. Under a gibbous moon horse and rider spanceled to their shadows on the snowblue ground and in each flare of lightning as the storm advanced those selfsame forms rearing with a terrible redundancy behind them like some third aspect of their presence hammered out black and wild upon the naked grounds. They rode on. They rode like men invested with a purpose whose origins were antecedent to them, like blood legatees of an order both imperative and remote. For although each man among them was discrete unto himself, conjoined they made a thing that had not been before and in that communal soul were wastes hardly reckonable more than those whited regions on old maps where monsters do live and where there is nothing other of the known world save conjectural winds.
