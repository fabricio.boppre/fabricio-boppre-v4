---
title: With the earth
date: 2020-02-28
credit: Walt Whitman
---

Now I see the secret of the making of the best persons: it is to grow in the open air and to eat and sleep with the earth.
