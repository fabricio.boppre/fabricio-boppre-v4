---
title: "A formative part"
date: 2020-06-08
credit: "Geoff Dyer in *Zona*"
---

I suspect it is rare for anyone to see their — what they consider to be _the_ — greatest film after the age of thirty. After forty it’s extremely unlikely. After fifty, impossible. The films you see as a child and in your early teens — _Where Eagles Dare_, _The Italian Job_ — have such a special place in your affections that it’s all but impossible to consider them objectively (you have, moreover, no desire to do so). To try to disentangle their individual merits or shortcomings, to see them as a disinterested adult, is like trying to come to a definitive assessment of your own childhood: impossible because what you are contemplating and trying to gauge is a formative part of the person attempting the assessment.
