---
title: A new world is only a new mind
date: 2023-07-24
credit: Allen Ginsberg on what we should do about the ecological crisis (*Playboy*, April 1969, copied from 
  [here](https://www.playboy.com/read/the-playboy-interview-with-allen-ginsberg))
---
The first step toward a cure for the sickness is to realize it's there. The robot standardization of American consciousness is one side-effect feedback from a greedy, defective technology, just as ecological disorder is another feedback, and these systemic disorders reinforce each other fatally unless there is complete metabolic change. One could argue with the patient for years—forever. But the fact is that he is his own disease. That's what he must be taught to recognize. What we must first realize is the fact of our own diminished consciousness. "A new world is only a new mind," as William Carlos Williams said. Being willing to solve problems depends mostly on being aware that they exist.
