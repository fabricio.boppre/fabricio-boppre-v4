---
title: "Ravens"
date: 2020-10-05
credit: "Ted Hughes"
---

As we came through the gate to look at the few new lambs  
On the skyline of lawn smoothness,  
A raven bundled itself into air from midfield  
And slid away under hard glistenings, low and guilty.  
Sheep nibbling, kneeling to nibble the reluctant nibbled grass.  
Sheep staring, their jaws pausing to think, then chewing again,  
Then pausing. Over there a new lamb  
Just getting up, bumping its mother’s nose  
As she nibbles the sugar coating off it  
While the tattered banners of her triumph swing and drip from her rear-end.  
She sneezes and a glim of water flashers from her rear-end.  
She sneezes again and again, till she’s emptied.  
She carries on investigating her new present and seeing how it works.

Over here is something else. But you are still interested  
In that new one, and its new spark of voice,  
And its tininess.  
Now over here, where the raven was,  
Is what interests you next. Born dead,  
Twisted like a scarf, a lamb of an hour or two,  
Its insides, the various jellies and crimsons and transparencies  
And treads and tissues pulled out  
In straight lines, like tent ropes  
From its upward belly opened like a lamb-wool slipper,  
The fine anatomy of silvery ribs on display and the cavity,  
The head also emptied through the eye-sockets,  
The woolly limbs swathed in birth-yolk and impossible  
To tell now which in all this field of quietly nibbling sheep  
Was its mother. I explain  
That it died being born. We should have been her, to help it.  
So it died being born. ‘And did it cry?’ you cry.  
I pick up the dangling greasy weight by the hooves soft as dogs’ pads  
That had trodden only womb-water  
And its raven-drawn strings dangle and trail,  
Its loose head joggles, and ‘Did it cry?’ you cry again.  
Its two-fingered feet splay in their skin between the pressures  
Of my fingers and thumb. And there is another,  
Just born, all black, splaying its tripod, inching its new points  
Towards its mother, and testing the note  
It finds in its mouth. But you have eyes now  
Only for the tattered bundle of throwaway lamb.  
‘Did it cry?’ you keep asking, in a three-year-old field-wide  
Piercing persistence. ‘Oh yes’ I say ‘it cried.’

Though this one was lucky insofar  
As it made the attempt into a warm wind  
And its first day of death was blue and warm  
The magpies gone quiet with domestic happiness  
And skylarks not worrying about anything  
And the blackthorn budding confidently  
And the skyline of hills, after millions of hard years,  
Sitting soft.
