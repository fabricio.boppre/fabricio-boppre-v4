---
title: Already
date: 2019-10-16
credit: Suketu Mehta in [*Immigration panic&#58; how the west fell for manufactured rage*](https://www.theguardian.com/uk-news/2019/aug/27/immigration-panic-how-the-west-fell-for-manufactured-rage) (*The Guardian*, August 2019)
---

The economist Jennifer Hunt tells a story about visiting Germany recently and listening to people making the liberal argument against letting in refugees: “If we let these people in, we’ll have the far right in government.” Hunt’s response: “If you don’t let these people in, you’ve already become a far-right government.”
