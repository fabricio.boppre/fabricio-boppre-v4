---
title: "Super-homens"
date: 2020-08-24
credit: "George Orwell em *O que é fascismo? e outros ensaios* (traduzido por Paulo Geiger)"
---

Não é de admirar que Hitler, a partir do momento em que chegou ao poder, tenha banido os filmes de Chaplin na Alemanha! A semelhança entre os dois homens (quase como a de gêmeos, é interessante lembrar) é cômica, especialmente nos movimentos rígidos de seus braços. E não é de espantar que escritores pró-fascistas como Wyndham Lewis e Roy Campbell tenham sempre perseguido Chaplin com um ódio tão peculiar e tão peçonhento! Do ponto de vista de quem acredita em super-homens, é um acidente dos mais desastrosos que o maior de todos os super-homens seja quase um sósia de um absurdo judeuzinho enjeitado com uma tendência a cair em baldes de cal. É o tipo de fato que deveria ser obscurecido. No entanto, felizmente, não pode ser obscurecido, e a sedução do poder político ficará um pouquinho mais fraca para todo ser humano que vir esse filme.
