---
title: Dois metros de terra
credit: Philip Roth em *A Pastoral Americana* (traduzido por Rubens Figueiredo)
---

Será que estou errado ao pensar que adorávamos viver ali? Nenhuma ilusão é mais corriqueira do que aquela que a nostalgia nos inspira na velhice, mas será que estou completamente enganado ao pensar que viver como crianças de boas famílias na Florença do Renascimento não chegaria nem aos pés de ser criado no raio de alcance aromático dos barris de picles de Tabachnik? Será que estou errado em pensar que mesmo naquele tempo, que era o nosso vívido presente, a plenitude da vida fazia vibrar nossas emoções a um grau extraordinário? Algum outro lugar, desde então, conseguiu inundar nossa mente com um mar de detalhes como aquele? O detalhe, a imensidão do detalhe, a força do detalhe, o peso do detalhe — a rica infinidade de detalhes que nos rodeava em nossa juventude, como os dois metros de terra que serão despejados em nossa cova quando estivermos mortos.
