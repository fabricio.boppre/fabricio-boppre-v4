---
title: Não ser
date: 2019-09-25
credit: De uma das reflexões de Sêneca compiladas no volume *Aprendendo a viver*
---

Qualquer coisa que tenha existido antes de nós é morte. Que importa, então, não começar a ser ou deixar de ser, se ambas as coisas têm o mesmo efeito, isto é, o não ser?
