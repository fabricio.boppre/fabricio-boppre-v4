---
title: Um haicai
date: 2022-12-08T19:09:17.849Z
credit: Victor Anselmo Costa (colhido da coleção *RÉS | CHÃO*, da editora
  [Casatrês](https://casatreseditora.art.br))
---
as coisas que afundam  
com as águas se confundem  
— pousadas no infinito