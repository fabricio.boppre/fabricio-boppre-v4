---
title: Terreno alagado
date: 2019-03-08
credit: Dos diários de Alan Bennett, publicado da revista piauí nº 146
---

Sei tão poucas coisas que escrever é como atravessar um terreno alagado, pulando de um pedacinho de terra para outro, tentando não molhar os pés (ou não tomar ovos na cara). Claro que de longe ninguém enxerga que o chão é pantanoso, e de longe também os movimentos das pessoas parecem mais suaves, as hesitações ficam diminuídas. Daqui a cinquenta anos, os saltos angustiados podem parecer passos confiantes. Mas será que vai ter alguém olhando?
