---
title: "Abandonar-se"
date: 2022-04-03
credit: "Andrei Tarkovski em *Esculpir o Tempo* (traduzido por Jefferson Luiz Camargo)"
---

Durante o processo de elaboração de um roteiro, eu sempre tentava obter em minha mente um quadro exato do filme, e até mesmo dos cenários. Atualmente, porém, estou mais propenso a trabalhar uma cena ou tomada apenas em termos muito gerais, para que elas surjam espontaneamente durante as filmagens, pois a vida característica do lugar onde se desenvolve ação, a atmosfera do set e o estado de espírito dos atores podem sugerir novas estratégias, surpreendentes e inesperadas. A imaginação é menos rica que a vida. E, hoje em dia, sinto com intensidade cada vez maior que ideias e estados de espírito não devem ser determinados antecipadamente. É preciso saber abandonar-se à atmosfera da cena e lidar com o set com a mente aberta. Já houve época em que eu não conseguia começar a filmar antes de ter elaborado um projeto completo do episódio; agora, porém, vejo tal procedimento como uma coisa abstrata, que cerceia a imaginação. Talvez fosse o caso de parar de pensar nisso por algum tempo.
