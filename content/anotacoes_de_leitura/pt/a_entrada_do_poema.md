---
title: À entrada do poema
date: 2019-07-23
credit: José Saramago em *Cadernos de Lanzarote*
---

Quem lê poesia, lê para quê? Para encontrar, ou para encontrar-se? Quando o leitor assoma à entrada do poema, é para conhecê-lo, ou para reconhecer-se nele? Pretende que a leitura seja uma viagem de descobridor pelo mundo do poeta, como tantas vezes se tem dito, ou, mesmo sem o querer confessar, suspeita que ela não será mais do que um simples pisar novo das suas próprias e conhecidas veredas? Não serão o poeta e o leitor como dois mapas de estradas de países ou regiões diferentes que, ao sobrepor-se, um e outro tornados transparência pela leitura, se limitam a coincidir algumas vezes em troços mais ou menos longos de caminho, deixando inacessíveis e secretos espaços de comunicação por onde apenas circularão, sem companhia, o poeta no seu poema, o leitor na sua leitura? Mais brevemente: que compreendemos nós, de facto, quando procuramos apreender a palavra e o espírito poéticos?
