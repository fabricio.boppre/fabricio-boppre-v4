---
title: Incongruência
date: 2019-05-12
credit: Lorenzo Mammì em [*Duas meninas*](https://piaui.folha.uol.com.br/materia/duas-meninas-2/), publicado na revista piauí nº 150
---

Talvez seja justamente esse o ponto do qual devemos partir: se algo se instalou no quadro, é da ordem da incongruência. Quem sabe, então, _Rosa e Azul_ não diga alguma coisa sobre a relação entre arte e história, não como simples oposição, como se a arte fosse um éden de onde o anjo da história nos expulsou, e sim como uma fratura interna, uma impossibilidade do pensamento. A história em que a obra continua vivendo a penetra, expõe contradições que nem sequer estavam ou pareciam estar ali à época. A pintura, por sua vez, reescreve a história, julga o futuro pelo passado, de maneira que tudo o que aconteceu após sua feitura se integra à sua constituição e adquire ali um sentido mais evidente. Auschwitz racha a superfície esfuziante de _Rosa e Azul_, e _Rosa e Azul_ condena Auschwitz de forma bem mais intensa e premente do que o repúdio que já nos acostumamos, quase automaticamente, a proferir. A incongruência – essa incongruência – se torna a razão de ser do quadro, lhe proporciona um sentido que não pretendia ter, mas que parece emergir dele como de um ato falho. A incongruência, quase outro nome da tragédia, nos obriga a recontar histórias.
