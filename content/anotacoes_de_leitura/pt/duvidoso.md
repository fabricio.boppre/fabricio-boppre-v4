---
title: Duvidoso
date: 2022-04-11T16:40:16.758Z
credit: George Orwell em *O que é fascismo? e outros ensaios* (traduzido por Paulo Geiger)
---

É duvidoso achar que uma prolongada preparação para a guerra seja, em termos morais, em qualquer aspecto melhor do que a guerra em si mesma; até existem motivos para se pensar que seja ligeiramente pior.
