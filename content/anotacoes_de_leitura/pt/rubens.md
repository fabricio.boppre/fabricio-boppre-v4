---
title: Rubens
credit: Dos diários de Jack Kerouac (traduzido por Edmundo Barreiros)
---

Domingo, 22 de maio de 1949: Fiz uma caminhada até Morrison Rd. para comprar este caderno e tomei uma cerveja numa grande tarde de domingo num bar de beira de estrada. Como as tardes de domingo são menos tristes no Oeste. Sentei-me perto da porta dos fundos e escutei música do coração dos Estados Unidos e contemplei os campos verdes dourados e as grandes montanhas. Caminhando pelos campos com meu cadernos eu poderia ter sido Rubens, e este lugar, minha Holanda. Voltei para casa, comi e fiz anotações preliminares à noite. Comecei On the Road lá em Ozone, e aqui está difícil. Passei um ano inteiro escrevendo antes de começar The Town and the City (1946) — mas isso não deve acontecer outra vez. Escrever, agora, é o meu trabalho, tanto no mundo quanto nas minhas "charnecas interiores" — então preciso me adiantar. Planejei um início anterior às 8.000 palavras já escritas nas duas primeiras semanas de maio em N.Y. Fui para cama depois da meia-noite e li um romance barato de faroeste.
