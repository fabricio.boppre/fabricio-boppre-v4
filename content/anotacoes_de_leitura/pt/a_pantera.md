---
title: A Pantera (No Jardim des Plantes, Paris)
credit: Rainer Maria Rilke (traduzido por José Paulo Paes)
---

Seu olhar, de tanto percorrer as grades,  
está fatigado, já nada retém.  
E como se existisse uma infinidade  
de grades e mundo nenhum mais além.

O seu passo clássico e macio, dentro  
do círculo menor, a cada volta urde  
como que uma dança de força: no centro  
delas, uma vontade maior se aturde.

Certas vezes, a cortina das pupilas  
ergue-se em silêncio. - Uma imagem então  
penetra, a calma dos membros tensos trilha -  
e se apaga quando chega ao coração.
