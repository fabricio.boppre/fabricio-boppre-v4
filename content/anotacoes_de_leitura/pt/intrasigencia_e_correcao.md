---
title: "Intransigência & correção"
date: 2021-08-27
credit: "Edward W. Said em *Reflexões sobre o exílio* (tradução de Pedro Maia Soares)"
---

James Joyce escolheu o exílio, para dar força à sua vocação artística. De um modo estranhamente eficaz — como Richard Ellmann mostrou em sua biografia do romancista irlandês —, Joyce arranjou uma querela com a Irlanda e a manteve viva, de maneira a sustentar a mais rigorosa oposição ao que era familiar. Ellmann diz que “sempre que suas relações com a terra natal corriam o perigo de melhorar, ele achava um novo incidente para solidificar sua intransigência e reafirmar a correção de sua ausência voluntária”. A ficção de Joyce tem a ver com o que, em uma carta, ele descreveu como o estado de ser “sozinho e sem amigos”. E, embora seja raro escolher o banimento como um modo de vida, ele compreendeu perfeitamente suas provações.
