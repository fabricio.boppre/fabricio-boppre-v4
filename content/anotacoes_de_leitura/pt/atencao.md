---
title: Atenção
date: 2020-07-13
credit: "Charlotte Joko Beck em *Nothing Special: Living Zen* (colhido do antigo
  site darma.info, que não está mais online)"
---

Há uma antiga história zen: um discípulo disse para o Mestre Ichu: “Por favor, me escreva algo de grande sabedoria”. Mestre Ichu pegou seu pincel e escreveu uma palavra: “Atenção”. O estudante disse: “Isso é tudo?”. O mestre escreveu: “Atenção, atenção”.

Por “atenção” nós podemos usar a expressão “estado desperto”. Atenção ou estado desperto é o segredo da vida, ou o coração da prática. Cada momento da vida é absoluto em si mesmo. É tudo que existe. Não há nada além do presente momento; não há passado, não há futuro; não há nada além disto. Então quando não prestamos atenção a “isto”, perdemos todo o quadro. E o conteúdo “disto” pode ser qualquer coisa.

“Isto” pode ser ajustar nossas almofadas de meditação, cortar uma cebola, visitar alguém que não gostaríamos de visitar. Não importa o conteúdo do momento, cada momento é absoluto. É tudo que existe e tudo que existirá para sempre. Se pudéssemos prestar atenção total, nunca nos irritaríamos. Se estamos irritados, é axiomático que não estamos prestando atenção. Se perdemos não apenas um momento, mas um momento depois do outro, então estamos com problemas.
