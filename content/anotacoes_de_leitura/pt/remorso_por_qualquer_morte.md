---
title: Remorso por qualquer morte
date: 2020-02-28
credit: Jorge Luis Borges
---

Livre da memória e da esperança,  
ilimitado, abstrato, quase futuro,  
o morto não é um morto: é a morte.  
Como o Deus dos místicos,  
de Quem devem negar-se todos os predicados,  
o morto ubiquamente alheio  
não é senão a perdição e ausência do mundo.  
Tudo nele roubamos,  
não lhe deixamos nem uma cor nem uma sílaba:  
aqui está o pátio que já não compartilham seus olhos,  
ali a calçada onde sua esperança espreitava.  
Até o que pensamos poderia estar pensando ele também;  
repartimos como ladrões  
o caudal das noites e dos dias.
