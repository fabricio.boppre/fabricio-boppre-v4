---
title: Ilusão
credit: Friedrich Wilhelm Nietzsche no aforismo número 28 de *Humano, Demasiado Humano*
---

Quem nos desvendasse a essência do mundo, nos causaria a todos a mais incômoda desilusão. Não é o mundo como coisa em si, mas o mundo como representação (como erro) que é tão rico em significado, tão profundo, maravilhoso, postador de felicidade e infelicidade.
