---
title: O crânio do linguado
date: 2019-04-08
credit: Richard Dawkins em *O Relojoeiro Cego* (traduzido por Laura Teixeira Motta)
---

O crânio inteiro do linguado contém o testemunho desvirtuado e deturpado de suas origens. Sua própria imperfeição é um poderoso indício de sua história antiquíssima, uma história de mudança passo a passo e não de um design deliberado. Nenhum designer sensato teria concebido tamanha monstruosidade se tivesse carta branca para criar um linguado em uma prancheta de desenho em branco. Desconfio que muitos designers sensatos pensariam em algo mais na linha das arraias. Mas a evolução nunca parte de uma prancheta de desenho em branco. Tem de começar do que já está disponível. No caso dos ancestrais das arraias, foram os tubarões de nado livre. Os tubarões em geral não são achatados lateralmente, como os peixes teleósteos de nado livre. Na verdade, os tubarões já são ligeiramente achatados da barriga à cauda. Isto significa que, quando alguns tubarões primitivos foram para o fundo do mar, ocorreu uma progressão suave e fácil para a forma da arraia, com cada intermediário constituindo um pequeno aperfeiçoamento, considerando as condições do fundo do mar, em relação a seu predecessor menos achatado.
