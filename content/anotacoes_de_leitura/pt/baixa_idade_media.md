---
title: Baixa Idade Média
date: 2019-07-23
credit: Don DeLillo em *Submundo* (traduzido por Paulo Henriques Britto)
---

E as pessoas na rua, quando foi que Klara começou a perceber que as pessoas falavam sozinhas, falavam alto, tantas pessoas e de repente, ou então faziam ameaças, ou andavam gesticulando, de modo que as ruas estavam ficando com um ar de Baixa Idade Média, o que talvez indicasse que seria necessário aprender de novo como é que se vive entre os loucos.
