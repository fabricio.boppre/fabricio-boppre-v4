---
title: Auto-ilusão
date: 2022-10-24T11:40:26.564Z
credit: G. K. Chesterton em *O segredo do padre Brown* (traduzido por Lucia Santaella)
---
Coloquei de um modo impróprio, mas é verdade. Nenhum homem é, de fato, bom, enquanto não souber quão mal ele é, ou poderia ser; enquanto não tiver se dado conta exatamente de quanto direito tem para todo o seu esnobismo, seu escárnio ao falar "criminosos", como se fossem macacos numa floresta, a dez mil milhas de distância; enquanto não se livrar de toda a auto-ilusão suja de falar sobre tipos baixos e crânios deficientes; enquanto não espremer para fora de sua alma até a última gota do óleo dos Fariseus; enquanto sua única esperança não for, de um modo ou outro, a de ter capturado um criminoso, e mantê-lo são e salvo sob seu próprio chapéu.