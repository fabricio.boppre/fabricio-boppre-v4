---
title: Vai
date: 2022-01-15
credit: Olga Tokarczuk em *Correntes* (tradução de Olga Bagińska-Shinzato)
---

Ela vestiu calças e uma blusa de linho pretas. Calçou os sapatos e pendurou a bolsa no ombro. Agora está parada, imóvel no corredor, e ela própria não sabe por quê. Em sua família, se costuma dizer que é preciso sentar por um minuto antes de se lançar em qualquer tipo de viagem — um antigo costume dos confins do Polônia oriental; mas aqui, neste pequeno vestíbulo, não há nenhuma cadeira, nenhum lugar para sentar. Então ela ficou em pé acertando seu relógio interno, seu cronômetro interno, por assim dizer, cosmopolita, esse cronômetro de carne e osso que tiquetaqueia surdamente ao ritmo da sua respiração. E de repente ela se recompõe, pega a mala pela alça feita uma criança que havia se distraído e abre a porta. Já está na hora de ir. Está pronta. Vai.
