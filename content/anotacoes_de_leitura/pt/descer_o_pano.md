---
title: Descer o pano
date: 2017-09-30
credit: Arthur Schopenhauer em *O Mundo como Vontade e Representação*
---

Todo poema épico e dramático só pode representar uma disputa, um esforço, uma luta pela felicidade; uma felicidade nunca duradoura e completa. Ele conduz o seu herói através de mil perigos e dificuldades até o objetivo; tão logo ele é atingido, urge fazer descer o pano; porque agora não restaria nada a fazer senão mostrar que o fulgurante objetivo no qual o herói esperava encontrar a felicidade só fizera desapontá-lo, e que depois de consegui-lo ele não estava em situação melhor do que antes.
