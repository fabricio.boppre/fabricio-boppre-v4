---
title: Inútil
date: 2019-08-13
credit: José Saramago em *Cadernos de Lanzarote*
---

Para o ABC Cultural escrevi, sobre Sarajevo, estas linhas que me foram pedidas: "Paul Valéry disse, um dia: 'Nós, civilizações, sabemos agora que somos mortais.' Importa pouco, para o caso, averiguar qual fosse o agora de Valéry: talvez a Primeira Guerra Mundial, talvez a Segunda. Do que ninguém tem dúvida é que a civilização que temos sido, não só era mortal, como está morta. E não só está morta, como decidiu, nos seus últimos dias, demonstrar até que ponto foi inútil. A proclamação dessa inutilidade está a ser feita em Sarajevo (e em quantos Sarajevos mais?) perante a cobardia da Europa política, perante, também, o egoísmo dos povos da Europa, perante o silêncio (ressalvadas fiquem as excepções) daqueles que fazem do pensar ofício e ganha-pão. A Europa política ensinou aos povos da Europa o refinamento do egoísmo. Compete aos intelectuais europeus, regressando à rua, ao protesto e à acção, escrever ainda uma última linha honrada no epitáfio desta civilização. Deste imenso Sarajevo que somos."
