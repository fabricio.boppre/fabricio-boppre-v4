---
title: A voz da floresta
date: 2023-01-06T21:39:03.207Z
credit: Davi Kopenawa (redigido por Bruce Albert) em *A queda do céu*
---
É por isso que devemos nos recusar a entregar nossa floresta. Não queremos que se torne uma terra nua e árida cortada por córregos lamacentos. Seu valor é alto demais para ser comprada por quem quer que seja. *Omama* disse a nossos ancestrais para viverem nela, comendo seus frutos e seus animais, bebendo a água de seus rios. Nunca disse a eles para trocarem a floresta e os rios por mercadoria ou dinheiro! Nunca os ensinou a mendigar arroz, peixe em lata de ferro ou cartuchos! O sopro de nossa vida vale muito mais! Para saber disso, não preciso ficar com os olhos cravados em peles de imagens, como fazem os brancos. Basta-me beber *yãkoana* e sonhar escutando a voz da floresta e os cantos dos *xapiri*.