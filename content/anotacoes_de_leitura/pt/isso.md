---
title: Isso
date: 2020-11-26
credit: Lobzang Jivaka em *The Life of Milarepa* (colhido do antigo site
  darma.info, que não está mais online)
---

O Buda, muito longe de negar que havia um Absoluto, garantiu que aqueles que alcançassem a iluminação deveriam se fundir com Isso e assim perceber a Realidade em oposição ao mundo das ilusões e dos fenômenos. O que ele realmente disse, contudo, que tem levado pessoas a acusarem-no de ateísmo, é que não temos nenhum meio de expressar qualquer coisa sobre Isso. Palavras pertencem ao universo dos fenômenos e são aplicáveis apenas à ele. Quando alguém vai além dos fenômenos, em direção à Realidade, palavras precisam obrigatoriamente ser deixadas para trás. Nenhum ensinamento, nenhuma descrição, nenhum pensamento podem expressar o Absoluto — mas podemos experimentá-lo, se suficientemente evoluídos.

O que Buda combateu foram as numerosas tentativas que foram feitas, estão sendo feitas e continuarão a ser feitas, de dizer que o Absoluto é isso ou aquilo, um Deus pessoal, um Criador, um Deus-Pai. Ele insistentemente recusou responder qualquer pergunta sobre o assunto porque isso era inexprimível em palavras. Ele não iria permitir a seus discípulos imaginar um Absoluto a semelhança deles, como é a tendência do homem em todo lugar. Ele assinalou sutilmente que é melhor se ajustar para tentar alcançar a iluminação e, assim, experimentar o Absoluto por si próprio, em vez de perder tempo tentando ineficazmente falar sobre isso, já que nada que possa ser dito sobre Isso pode ser verdade em absoluto. Palavras iriam inevitavelmente modificá-Lo e moldá-Lo, resultando no máximo em uma aproximação grosseira. Palavras podem ser verdadeiras apenas em certo nível, mas apenas nesse nível, portanto serão apenas verdades relativas. Assim, como um entendimento que só funciona por meio de palavras pode conter o que não pode ser colocado em palavras? Apenas pela experiência direta.
