---
title: "Cantos de xapiri"
date: 2020-12-16
credit: "Davi Kopenawa (redigido por Bruce Albert) em *A queda do céu*"
---

Os xapiri se deslocam flutuando nos ares a partir de seus espelhos, para vir nos proteger. Ao chegarem, nomeiam em seus cantos as terras distantes de onde vêm e as que percorreram. Evocam os locais onde beberam a água de um rio doce, as florestas sem doenças onde comeram alimentos desconhecidos, os confins do céu onde não há noite e ninguém jamais dorme. Quando o espírito papagaio termina seu canto, o espírito anta começa o dele; depois é a vez do espírito onça, do espírito tatu-canastra e de todos os ancestrais animais. Cada um deles primeiro oferece suas palavras, para então perguntar por que seu pai os chamou e o que devem fazer.
