---
title: Desamparo
date: 2022-04-18
credit: Sándor Lénárd em *O Vale do Fim do Mundo* (traduzido por Paulo Schiller)
---

Parece incompreensível que justamente o europeu, que conheceu a profundeza dos horrores, conheceu o medo da morte e a fome, golpeado pelas pragas da civilização, quando as bençãos da cultura o abandonaram havia muito tempo, continue querendo viver entre estradas sinalizadas e proibições. Anseia pelo escritório, pela fábrica. Deseja se queixar do superior malvado, do subordinado estúpido, deseja ir ao cinema para que possa ver as paisagens onde não teria coragem e seria incapaz de viver.

Não, não é incompreensível. O europeu é tão desamparado que não pode escolher a liberdade infinita da vida simples, elementar. A fraqueza do indivíduo cria a horda; também nessas horas, políticos educados falam de massa. Os fracos anseiam pela camisa colorida, pelas águias costuradas nos uniformes, pelo barrete negro, pela bota, porque sem eles se sentem ainda mais fracos. O europeu após a Primeira Guerra ou, o que dá no mesmo, antes da Segunda Guerra foram tão amorfos que alguns bufões pretenciosos puderam sentar-se em seus ombros — e aqui eles são tão fracos que as pernas não os levam adiante quando o bonde para.
