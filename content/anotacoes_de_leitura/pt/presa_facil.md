---
title: " Presa fácil"
date: 2021-04-05
credit: "Joseph Brodsky em *Para agradar a uma sombra* (na tradução de Sérgio Flaksman)"
---

Se os poetas têm alguma obrigação em relação à sociedade, é a de escrever bem. Estando em minoria, não têm outra escolha. Se não cumprirem com esta obrigação, submergem no esquecimento. Maioria por definição, a sociedade acredita que tem outras opções além de ler versos, por mais bem escritos que eles possam ser. Quando não conseguem fazê-lo, o resultado é descerem àquele nível de elocução em que a sociedade se transforma em presa fácil para um demagogo ou um tirano. É este o equivalente do esquecimento para a sociedade; um tirano, é claro, sempre pode tentar salvar seus súditos de si mesmo por meio de algum banho de sangue espetacular.
