---
title: Censo
date: 2017-03-25
credit: Wisława Szymborska (publicado no Brasil no volume *Um amor feliz*, traduzido por Regina Przybycien)
---

Na colina onde ficava Troia  
foram escavadas sete cidades.  
Sete cidades. Seis a mais  
para uma única epopéia.  
Que fazer com elas? Que fazer?  
Arrebentam os hexâmetros,  
um tijolo afabular espia pelas brechas,  
no silêncio do filme mudo, muros derrubados,  
vigas queimadas, correntes rompidas,  
cântaros esvaziados até a última gota,  
amuletos da fertilidade, caroços de fruta  
e caveiras tangíveis como a lua de amanhã.

Nossa dose de antiguidade vai crescendo,  
fica apinhada de gente,  
inquilinos brutais se empurram na história,  
hordas de carne para a espada,  
extras de Heitor iguais a ele em bravura,  
milhares e milhares de rostos singulares,  
cada um o primeiro e o último no tempo,  
e em cada rosto dois olhos sem par.  
Era tão fácil não saber nada sobre isso,  
tão comovedor, tão amplo.

Que fazer com eles? O que lhes dar?  
Algum século pouco povoado até agora?  
Um pouco de apreço pela arte da ourivesaria?  
Pois é muito tarde para o juízo final.

Nós, três bilhões de juízes,  
temos nossos problemas,  
nossas turbas inarticuladas,  
estações, arquibancadas, procissões,  
incontáveis números de estranhas ruas, andares, paredes.  
Desencontramo-nos para sempre nas grandes lojas  
comprando um jarro novo.  
Homero trabalha num instituto de estatística.  
Ninguém sabe o que ele faz em casa.
