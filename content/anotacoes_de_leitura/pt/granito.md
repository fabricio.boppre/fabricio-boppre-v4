---
title: Granito
credit: Friedrich Wilhelm Nietzsche no aforismo número 43 de *Humano, Demasiado Humano*
---

Devemos pensar nos homens que hoje são cruéis como estágios remanescentes de culturas passadas: a cordilheira da humanidade mostra abertamente as formações mais profundas, que em geral permanecem ocultas. São homens atrasados, cujo cérebro, devido a tantos acasos possíveis na hereditariedade, não se desenvolveu de forma vária e delicada. Eles mostram o que todos nós fomos, e nos infundem pavor: mas eles próprios são tão responsáveis como um pedaço de granito é responsável pelo fato de ser granito. Em nosso cérebro também devem se achar sulcos e sinuosidades que correspondem àquela mentalidade, assim como na forma de alguns órgãos humanos podem se achar lembranças do estado de peixe. Mas esses sulcos e sinuosidades já não são o leito por onde rola atualmente o curso de nosso sentimento.
