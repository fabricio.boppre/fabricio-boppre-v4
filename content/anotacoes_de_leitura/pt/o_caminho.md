---
title: O caminho
date: 2022-12-18T19:47:43.197Z
credit: Omar Khayyám (tradução de Christovam de Camargo e Ragy Basile)
---
Alguns, fanáticos,  
tendo feito calar  
a voz da razão,  
submetem-se obtusamente  
à inanidade das crenças  
e aos dogmas da religião.

Enquanto outros,  
perplexos, aturdidos,  
ficam indecisos  
entre clarões e trevas,  
entre aceitação e dúvida.  

Eis que, despertando-os,  
clama  
a voz estentórica de um fantasma:

Ó insensatos,  
eternamente ludibriados,  
quereis saber o caminho?  
O caminho não é este...  
Não é este,  
nem é aquele...