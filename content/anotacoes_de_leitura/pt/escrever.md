---
title: Escrever
credit: Marguerite Duras
---

A escrita é o desconhecido. Antes de escrever não sabemos nada acerca do que vamos escrever. Com toda a lucidez. É o desconhecido de nós mesmos, da nossa cabeça, do nosso corpo. Não é sequer uma reflexão, escrever é uma espécie de faculdade que temos ao lado da nossa pessoa, paralelamente a ela, de uma outra pessoa que aparece e que avança, invisível, dotada de pensamento, de cólera, e que, por vezes, pelos seus próprios fatos, está em perigo de perder a vida. Se soubéssemos alguma coisa do que vamos escrever, antes de o fazer, antes de escrever, nunca escreveríamos. Não valeria a pena. Escrever é tentar saber aquilo que escreveríamos se escrevêssemos — só o sabemos depois — antes, é a interrogação mais perigosa que nos podemos fazer. Mas é também a mais corrente.
