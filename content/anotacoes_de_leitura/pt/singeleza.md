---
title: "Singeleza"
date: 2020-06-20
credit: "Jorge Luis Borges"
---

Abre-se a cancela do jardim  
com a docilidade da página  
que uma freqüente devoção interroga  
e dentro os olhares  
não precisam deter-se nos objetos  
que já estão cabalmente na memória.  
Conheço os costumes e as almas  
e esse dialeto de alusões  
que todo agrupamento humano vai urdindo.  
Não necessito falar  
nem mentir privilégios;  
bem me conhecem aqueles que aqui me rodeiam,  
bem sabem minhas penas e minha fraqueza.  
Isso é alcançar o mais alto,  
o que talvez nos dará o Céu:  
não admirações nem vitórias  
mas simplesmente ser admitidos  
como parte de uma Realidade inegável,  
como as pedras e as árvores.
