---
title: Janelas vazadas
date: 2015-06-20
credit: W. G. Sebald em *Os emigrantes* (traduzido por José Marcos Mariani de Macedo)
---

Em dezembro de 1952 nós nos mudamos da aldeia de W. para a cidadezinha de S., a 19 quilômetos dali. Fiz a viagem na boléia do caminhão de mudanças Alpenvogel cor de vinho, observando intensamente as intermináveis fileiras de árvores da estrada que, cobertas de grossa geada, emergiam diante de nós no nevoeiro denso da manhã. O trajeto, que não durou mais de uma hora, me pareceu uma viagem por metade da terra. Quando finalmente atravessamos a ponte do Ach para entrar em S., naquele tempo uma pequena vila com talvez nove mil habitantes, tive a nítida sensação de que ali começaríamos uma movimentada vida nova, de cidade grande, cujos sinais indiscutíveis eu julgava reconhecer nas placas com o nome das ruas em esmalte azul, o imenso relógio na frente da velha estação de trem e a fachada, para mim extremamente imponente, do Wittelsbacher Hof Hotel. Mas o que me parecia especialmente promissor eram os escombros de prédios espalhados por ali, pois para mim, desde que estivera certa vez em Munique, nada se ligava tanto à palavra "cidade" quanto escombros, muros chamuscados e janelas vazadas por onde se podia ver o vazio.
