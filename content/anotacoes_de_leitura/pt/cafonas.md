---
title: Cafonas
date: 2024-12-26T13:11:37.782Z
credit: Miguel Lago em [Derrubem as
  estátuas](https://piaui.folha.uol.com.br/materia/derrubem-as-estatuas/),
  publicado na revista piauí nº 168
---
A derrubada de estátuas é saudável e um símbolo de sociedades democráticas. A queda da ditadura soviética e do autoritarismo assassino que colonizou boa parte da Europa Central e do Leste Europeu costuma ser marcada pela derrubada das estátuas de Lênin. Em nenhum momento se propôs proibir seus livros ou se vilanizou o personagem histórico. Quem foi “apagado” não foi o pensador bolchevique, o líder da Revolução de 1917 e o chefe de Estado, mas o mito leninista que conferia legitimidade ao regime de terror que governava aqueles países. Não se trata de revisionismo histórico ou de julgamento moral de figuras do passado, mas sim de mitos sobre os quais poderes fáticos do presente justificam seu exercício de poder.

As estátuas expressam e alimentam a megalomania, a vontade de poder, uma série de valores negativos para uma sociedade democrática. Acredito que não se faz uma sociedade democrática com a presença delas. Sou inclusive favorável à derrubada de todas: não apenas dos escravocratas, dos colonialistas, dos ditadores. A ideia de estátua alimenta uma mitologia que nos faz depositar nossa fé em líderes despóticos, sejam eles eleitos ou não. Desde que a estátua não tenha nenhum valor artístico, poderia ser derrubada. A maioria delas é de péssimo gosto, aliás. Costumam ser obras neoclássicas cafonas. Às bonitas, reservemos uma galeria de algum museu.