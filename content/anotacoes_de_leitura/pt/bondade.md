---
title: Bondade
credit: Roberto Saviano sobre [Varlam Chalámov](https://pt.wikipedia.org/wiki/Varlam_Shalamov), extraído de um dos volumes de *Contos de Kolimá*
---

Chalámov consegue demonstrar a bondade do simples gesto no inferno cotidiano do gulag. Como a frase de um personagem de Vassili Grossman: "Eu não acredito no bem. Acredito na bondade". O bem é uma consideração metafísica, distante, geral, póstuma. A bondade é um espaço do presente. Do olhar-se nos olhos. De um momento. A bondade é humana, o bem é histórico. E, quando se fala de projeto histórico, de justiça, de felicidade como algo que transcende o humano, Chalámov sente um calafrio de medo. Sabe que se fala de algo que fará o homem sofrer, algo que passará por cima do homem.

Chalámov consegue demonstrar, através da observação da natureza, que resistir é possível. Em cada simples acontecimento há uma gota de possibilidade: a possibilidade da vida. Esse discurso nas páginas de Chalámov não é retórico. Não é nem mesmo religioso. Não é um desejo de crer num contexto em que tudo é desesperador e desumano. Trata-se, para ele, de uma busca. Estando em silêncio, lutando para comer. O orgulho da existência. A capacidade de não se deixar corromper pela necessidade. É possível continuar a ser humano mesmo naquelas condições, isto é possível conseguir. Eis a grandeza de Chalámov.
