---
title: Em primeiro lugar
credit: André Malraux
---

A verdade de um homem é em primeiro lugar aquilo que ele esconde.
