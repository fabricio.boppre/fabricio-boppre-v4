---
title: "sobre este site"
meta_description: "Como construí este website."
---

Este site foi construído com o framework [Next.js](https://nextjs.org) e está hospedado na plataforma [Vercel](https://vercel.com/). Todas as suas páginas são estáticas e pré-renderizadas no momento em que eu publico uma nova atualização (método conhecido como [Static Generation](https://nextjs.org/docs/basic-features/pages#static-generation-recommended)). Seu conteúdo está armazenado em arquivos [Markdown](https://daringfireball.net/projects/markdown/), sendo que para a seção _anotações de leitura_ eu conto com a ajuda do Netlify CMS para poder publicar novas anotações de qualquer lugar.

O código-fonte — escrito com o [VS Code](https://code.visualstudio.com) e disponível em um [repositório no GitLab](https://gitlab.com/fabricio.boppre/fabricio-boppre-v4) — é enxuto, [semântico](https://pt.wikipedia.org/wiki/Web_semântica) e [responsivo](https://en.wikipedia.org/wiki/Responsive_web_design).

As seções _portfólio_ e _ilustrações_ utilizam a biblioteca [Masonry](https://masonry.desandro.com) para montar seus mosaicos.

A fonte tipográfica utilizada é a [Libre Baskerville](https://fonts.google.com/specimen/Libre+Baskerville).
