---
title: "about this site"
meta_description: "How I built this website."
---

This website was built with [Next.js](https://nextjs.org) and is hosted on [Vercel](https://vercel.com/). All of its pages are static and pre-rendered the moment I update something (method known as [Static Generation](https://nextjs.org/docs/basic-features/pages#static-generation-recommended)). Its content is stored in [Markdown](https://daringfireball.net/projects/markdown/) files, and for the _reading notes_ section I use Netlify CMS to be able to publish new notes from anywhere.

The source code, written with [VS Code](https://code.visualstudio.com), is available in a [GitLab repository](https://gitlab.com/fabricio.boppre/fabricio-boppre-v4). It's lean, [semantic](https://en.wikipedia.org/wiki/Semantic_Web) and [responsive](https://en.wikipedia.org/wiki/Responsive_web_design).

Sections _portfolio_ and _illustrations_ use [Masonry](https://masonry.desandro.com) to assemble their mosaics.

The typographic font used is [Libre Baskerville](https://fonts.google.com/specimen/Libre+Baskerville).
