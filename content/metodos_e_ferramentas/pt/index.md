---
title: "métodos & ferramentas"
meta_description: "Alguns dos serviços e ferramentas que utilizo em meu trabalho."
---

A caixa de ferramentas de um web developer é algo em constante transformação. Também o é, consequentemente, sua literatura de apoio e demais materiais de referência.

Minha principal área de interesse e estudo é a organização de conteúdo na web. Como oferecer ao mundo de forma acessível e eficiente o conteúdo com o qual eu esteja trabalhando, como torná-lo atraente e corretamente integrado ao ambiente potencialmente infinito do qual ele fará parte, e como fazer deste ambiente — a [World Wide Web](https://webfoundation.org) — um lugar cada vez melhor: estas são algumas das questões e alguns dos princípios que norteiam meu trabalho.

Abaixo estão relacionados alguns dos sites que fazem parte do meu dia a dia: conceitos e linguagens de programação; tutoriais sobre como funciona a infra-estrutura da web; manuais de boas práticas e artigos especulativos sobre novas tendências; showcases e portfólios que me servem de inspiração; serviços de testes e validação; frameworks, bibliotecas e aplicativos que utilizo.

- **[W3C](https://www.w3.org)**: Site da comunidade internacional responsável pelo desenvolvimento dos padrões de desenvolvimento para a web.
- **[Semantic Web](https://www.w3.org/standards/semanticweb/)**: Discussões acerca do futuro da web e o conceito de "web of data", ou "semantic web".
- **[Jamstack](https://jamstack.org)**: Uma nova e mais eficiente arquitetura para projetos web.
- **[HTML5 Doctor](http://html5doctor.com)**: Site de referência para programação em HTML5.
- **[W3Schools - CSS3](https://www.w3schools.com/css/css3_intro.asp)**: Site de referência para a nova versão da linguagem de estilos na web, a CSS3.
- **[Laws of UX](https://lawsofux.com)**: Coleção de teorias e princípios para a construção de interfaces.
- **[MDN web docs](https://developer.mozilla.org/en-US/)**: Plataforma de documentos sobre todos os aspectos relativos ao desenvolvimento para a web (mantida pelo Mozilla).
- **[web.dev](https://web.dev)**: Outra plataforma de documentação, esta mantida pelo Google.
- **[The Modern Javascript Tutorial](https://javascript.info)**: Extensa documentação sobre um dos principais recursos para a construção de web sites, o JavaScript.
- **[React](https://reactjs.org)**: Biblioteca JavaScript que facilita e enriquece o desenvolvimento de interfaces.
- **[Astro](https://astro.build)**: [Static site generator](https://www.cloudflare.com/pt-br/learning/performance/static-site-generator/) que tem como vantagem a ótima performance e leveza dos web sites gerados.
- **[Next.js](https://nextjs.org)**: Framework React robusto que traz diversas inovações para a construção de web sites mais complexos.
- **[Cloudflare](https://www.cloudflare.com/)**: Plataforma que oferece muitos recursos e serviços para a construção e publicação de projetos web. Eu utilizo principalmente o serviço [Pages](https://pages.cloudflare.com).
- **[Vercel](https://vercel.com/home)**: Plataforma para a publicação de projetos construídos com as modernas ferramentas da arquitetura Jamstack.
- **[Netlify](https://www.netlify.com)**: Outra plataforma para publicação de projetos baseados na arquitetura Jamstack.
- **[Svelte](https://svelte.dev)**: Ferramenta para construção de interfaces similar ao React em alguns aspectos, mas com alguns pontos essenciais diferentes. Tenho me tornando grande fã.
- **[SvelteKit](https://kit.svelte.dev)**: Framework Svelte para a construção de web sites.
- **[Everyone has JavaScript, right?](https://kryogenix.org/code/browser/everyonehasjs.html)**: JavaScript é ótimo, mas gosto de sempre levar em conta este conselho.
- **[Modernizr](https://modernizr.com/)**: Ferramenta que auxilia a construção de uma experiência web padronizada levando-se em conta os diferentes contextos em que o site poderá ser visualizado (browsers diferentes, dispositivos diferentes, recursos diferentes, etc.)
- **[Bitbucket](https://bitbucket.org)**: Serviço de compartilhamento de código-fonte e trabalho colaborativo.
- **[GitLab](https://gitlab.com/)**: Outro repositório para código-fonte e trabalho colaborativo.
- **[Can I use...](https://caniuse.com)**: Serviço on-line essencial sobre a compatibilidade dos browsers com novas tecnologias.
- **[Solved by Flexbox](https://philipwalton.github.io/solved-by-flexbox/)**: Coleção de dicas e tutoriais sobre o Flexbox.
- **[Sass](http://sass-lang.com)**: Linguagem que extende as possibilidades do CSS.
- **[CakePHP](https://cakephp.org)**: Framework para projetos web de grande porte. Um velho favorito!
- **[WordPress](https://wordpress.org)**: Sistema de gerenciamento de conteúdo para projetos web de pequeno e médio porte. Um pouco ultrapassado, mas ainda uma boa ferramenta para algumas situações.
- **[Netlify CMS](https://www.netlifycms.org)**: Outro sistema de gerenciamento de conteúdo, este compatível com algumas técnicas mais modernas de web development.
- **[Dive into HTML5](https://diveintohtml5.info)**: Compilação de dicas e tutoriais sobre HTML5.
- **[Autodesk SketchBook](https://www.sketchbook.com/)**: Aplicativo para ilustração digital.
- **[Font Squirrel](https://www.fontsquirrel.com)**: Pesquisa e download de fontes tipográficas para projetos web.
- **[We Love Typography](http://welovetypography.com)**: Mais uma galeria de fontes tipográficas para a web.
- **[palleton.com](http://paletton.com/)**: Site para a criação de esquemas de cores.
- **[Webmaster Central Blog](https://webmasters.googleblog.com)**: Blog com dicas sobre a integração de sites aos serviços do Google.
- **[Advanced Common Sense](https://sensible.com)**: Site de Steve Krug, autor de um dos livros fundamentais sobre usabilidade na web, Don't Make Me Thing.
- **[Adobe](http://www.adobe.com)**: Site da fabricante de algumas ferramentas que utilizo, como Photoshop e Fireworks (no século passado eu também usava Flash).
- **[A List Apart](https://alistapart.com)**: Excepcional site com artigos e discussões sobre desenvolvimento para a web.
- **[StatCounter](https://gs.statcounter.com)**: Este site, com estatísticas diversas sobre como as pessoas navegam pela web, é uma ferramenta imprescindível.
- **[24 Ways](https://24ways.org)**: Outro site indispensável sobre desenvolvimento para a web.
- **[Snipcart](https://snipcart.com)**: Biblioteca JavaScript para ecommerce.
- **[Culture Code](https://culturedcode.com/)**: Site da empresa que desenvolve a ferramenta Things, que utilizo para a organização de projetos (utilizando a metodologia [GTD](https://gettingthingsdone.com)).
- **[Getting Real](https://gettingreal.37signals.com)**: Site sobre a metodologia de desenvolvimento web Getting Real.
- **[Pingdom Website Speed Test](https://tools.pingdom.com)**: Serviço on-line para testes de performance de web-sites.
- **[Panic](https://www.panic.com/)**: Site da empresa que desenvolve o editor/gerenciador de código-fonte da minha preferência, o Nova (além de terem lançado um belíssimo game, [Firewatch](https://www.firewatchgame.com)).
- **[Visual Studio Code](https://code.visualstudio.com/)**: Outro editor de código-fonte, que tenho utilizado mais frequentemente em projetos com [CI/CD](https://www.redhat.com/pt-br/topics/devops/what-is-ci-cd).
- **[Sketch](https://sketchapp.com)**: Minha ferramenta favorita para a criação de protótipos.
- **[CSS Zen Garden](http://www.csszengarden.com)**: Site sobre desenvolvimento de layouts para a web.
- **[Lodash](https://lodash.com)**: Biblioteca de utilidades para JavaScript.
- **[awwwards.](https://www.awwwards.com)**: Site sobre design, criatividade e inovação na internet.
- **[MySQL](https://www.mysql.com)**: Sistema de banco de dados da minha preferência.
- **[PHP](http://php.net)**: Site do PHP, linguagem de programação bastante popular.
- **[Learn UI Design](https://learnui.design/blog/)**: Blog sobre design e desenvolvimento de interfaces.
- **[CSS-TRICKS](https://css-tricks.com)**: Blog fundamental sobre design e desenvolvimento web.
- **[Smashing Magazine](https://www.smashingmagazine.com)**: Também este blog é muito bom.
- **[Markdown](https://daringfireball.net/projects/markdown/)**: Ferramenta de conversão de texto para HTML.
- **[MDX](https://mdxjs.com)**: MDX é um formato que aumenta as possibilidades do Markdown ao permitir a inclusão de componentes escritos em JSX.
- **[Rough Notation](https://roughnotation.com)**: Pequena biblioteca JavaScript para criar anotações animadas em textos.
- **[license](https://www.npmjs.com/package/license)**: Ferramenta para adicionar uma licença ao código-fonte de um projeto. Costumo utilizar para atribuir a [licença MIT](https://opensource.org/licenses/MIT) ao meus projetos open-source.
- **[AWS](https://aws.amazon.com)**: Plataforma que oferece dezenas de recursos e serviços para a construção e publicação de projetos web. Eu utilizo principalmente o serviço [Lambda](https://aws.amazon.com/lambda/) para a execução de serverless functions.
- **[Nhost](https://nhost.io)**: Plataforma que oferece diversos recursos e serviços para o backend de projetos web.
- **[Lighthouse](https://github.com/GoogleChrome/lighthouse)**: Ferramenta para a análise de performance e boas-práticas em projetos web.
- **[Website Carbon Calculator](https://www.websitecarbon.com)**: Serviço que estima o carbon footprint de um web-site e sugere ações para torná-lo menos prejudicial ao meio ambiente.
- **[Contrast Ratio](https://contrast-ratio.com)**: Serviço que avalia o contraste entre texto e cor de fundo, auxiliando assim na legibilidade de um web-site.
- **[Postman](https://www.postman.com)**: Platforma para o desenvolvimento e testes de APIs.
- **[JSON Editor Online](https://jsoneditoronline.org)**: Ferramenta online de edição e formatação de conteúdo no formato JSON.

Abaixo seguem alguns sites de artistas gráficos, ilustradores, editoras artesanais, etc. Não diretamente relacionados ao mundo do desenvolvimento para web, mas altamente inspiradores e estimulantes:

- **[Derek Riggs website and online portfolio](http://derek.server311.com/derekriggs.com/index.html)**: Portfólio do lendário ilustrador Derek Riggs.
- **[DeviantArt](http://www.deviantart.com)**: Galeria com obras de diversos artistas ao redor do mundo.
- **[Club of the Waves](https://clubofthewaves.com)**: Galeria com obras de artistas ligados à cultura surf ao redor do mundo.
- **[Anonymous Ink &amp; Idea](http://www.anonymousii.com)**: Portfólio do ilustrador Rich Knepprath.
- **[Monster Brains](http://monsterbrains.blogspot.com.br/)**: Galeria com obras de diversos artistas gráficos (antigos e modernos) que têm em comum a temática sobrenatural.
- **[50 Watts](http://50watts.com)**: Galeria com obras de diversos artistas gráficos ao redor do mundo.
- **[Wacom Gallery](http://gallery.wacom.com/)**: Galeria de obras criadas com as ferramentas para ilustração digital da Wacom.
- **[The Jazzy Blue Notes of Reid Miles](http://retinart.net/artist-profiles/jazzy-blue-notes-reid-miles/)**: Site sobre a obra de Reid Miles, fotógrafo e artista gráfico conhecido por seu trabalho junto à gravadora americana de jazz Blue Note.
- **[Hard Format](http://www.hardformat.org)**: Site dedicado à arte gráfica de discos.
- **[Leandro Lopes Ilustrações](http://leandrolopesilustracoes.com.br)**: Portfólio do amigo e ilustrador Leandro Lopes (desenvolvido por mim).
- **[RIDDICKART](http://riddickart.com)**: Portfólio do mestre Mark Riddick.
- **[Casatrês](https://casatreseditora.art.br)**: Site da editora artesanal Casatrês.
