---
title: "perfis, redes sociais & outros sites pessoais"
meta_description: "Meus perfis em alguns serviços e redes sociais que utilizo."
---

- **[TUIMP - The Universe in my Pocket](http://tuimp.org)**: Site sobre astronomia concebido pela astrônoma francesa Grażyna Stasińska, desenvolvido e gerenciado por mim, com conteúdo criado e traduzido por uma rede de astrônomos ao redor do mundo.

- **[Dying Days](http://dyingdays.net)**: Blog sobre música fundado por mim e alguns amigos em 1999.

- **[Raindrop.io - textos arquivados](https://raindrop.io/fabricio-boppre/archive-44348870)**: Uma seleção de textos que gostei e recomendo a leitura.

- **[Discogs](https://www.discogs.com/user/fabricio.boppre/collection)**: Minha coleção e minhas contribuições no maior banco de dados de discos da internet.

- **[last.fm](https://www.last.fm/user/fabricioboppre)**: Estatísticas sobre minhas audições de música em formato digital.

- **[ListenBrainz](https://listenbrainz.org/user/fabricioboppre/)**: Este é outro serviço que oferece estatísticas sobre minhas audições de música em formato digital, e eu até prefiro sua interface e organização, porém, infelizmente, ele não é compatível com todas as minhas fontes de música digital e portanto suas informações são incompletas.

- **[bandcamp](https://bandcamp.com/fabricio_boppre)**: Perfil no bandcamp, site de venda de música de artistas (em sua maioria) independentes.

- **[flickr](https://www.flickr.com/photos/fabricio-boppre/)**: Álbum de fotografias.

- **[Codecademy](https://www.codecademy.com/profiles/fabricio.boppre)**: Perfil no Codecademy, site de ensino de programação.

- **[Chess.com](https://www.chess.com/member/fabricio_boppre)**: Perfil no Chess.com, site para jogadores de xadrez.

- **[IMDb](http://www.imdb.com/user/ur5280222/ratings)**: Lista de filmes assistidos e avaliados no IMDb.

- **[Mubi](https://mubi.com/users/12941701)**: Perfil no Mubi, meu serviço de streaming de filmes favorito.

- **[Steam](http://steamcommunity.com/id/fabricio_boppre)**: Community Perfil no Steam, site de venda e plataforma de games.

- **[GOG.com](https://www.gog.com/u/fabricio.boppre)**: Perfil no GOG, site de venda de games sem DRM.

- **[Behance](https://www.behance.net/fabricio-boppre)**: Perfil no Behance, portólio on-line para artistas gráficos e web designers.

- **[Bitbucket](https://bitbucket.org/fabricioboppre/)**: Perfil no Bitbucket, plataforma de compartilhamento de código-fonte.

- **[GitLab](https://gitlab.com/fabricio.boppre/)**: Perfil no GitLab, outra plataforma de compartilhamento de código-fonte.

- **[LinkedIn](https://www.linkedin.com/in/fabricioboppre)**: Perfil no LinkedIn.

- **[Workana](https://www.workana.com/freelancer/2ee30a7bbe77afa605567a0cd339030c)**: Perfil no Workana.

- **[Upwork](https://www.upwork.com/freelancers/~013df26040f925b7c9)**: Perfil no Upwork.

- **[Freelancer.com](https://www.freelancer.com/u/fabricioboppre)**: Perfil no Freelancer.com.

- **[Kickstarter](https://www.kickstarter.com/profile/fabricioboppre)**: Projetos que apóio ou apoiei no Kickstarter.
