---
title: "profiles, social networks & other personal stuff"
meta_description: "My profiles on some services and social networks I use."
---

- **[TUIMP - The Universe in my Pocket](http://tuimp.org)**: Website idealized by French astronomer Grażyna Stasińska, developed and managed by me, with content created and translated by a network of astronomers around the world.

- **[Dying Days](http://dyingdays.net)**: Music blog founded by me and some friends in 1999.

- **[Raindrop.io - archived texts](https://raindrop.io/fabricio-boppre/archive-44348870)**: A selection of texts that I liked and recommend reading.

- **[Discogs](https://www.discogs.com/user/fabricio.boppre/collection)**: My collection and contributions to the biggest record database on the internet.

- **[last.fm](https://www.last.fm/user/fabricioboppre)**: Statistics about the (digital) music I listen to.

- **[ListenBrainz](https://listenbrainz.org/user/fabricioboppre/)**: This is another service that offers statistics about my music listening, and I prefer its interface and organization, but unfortunately it isn't compatible with all my digital music sources and therefore its information is incomplete.

- **[bandcamp](https://bandcamp.com/fabricio_boppre)**: Profile on bandcamp, a website for selling music by (mostly) independent artists.

- **[flickr](https://www.flickr.com/photos/fabricio-boppre/)**: Photo album.

- **[Codecademy](https://www.codecademy.com/profiles/fabricio.boppre)**: Profile on Codecademy, website for teaching programming.

- **[Chess.com](https://www.chess.com/member/fabricio_boppre)**: Profile on Chess.com, website for chess players.

- **[IMDb](http://www.imdb.com/user/ur5280222/ratings)**: List of movies watched and rated on IMDb.

- **[Mubi](https://mubi.com/users/12941701)**: Profile on Mubi, my favorite movie streaming service.

- **[Steam](http://steamcommunity.com/id/fabricio_boppre)**: Profile on Steam, a video game platform.

- **[GOG.com](https://www.gog.com/u/fabricio.boppre)**: Profile on Steam, another video game platform.

- **[Behance](https://www.behance.net/fabricio-boppre)**: Profile on Behance, an online portfolio for graphic artists and web designers.

- **[Bitbucket](https://bitbucket.org/fabricioboppre/)**: Profile on Bitbucket, source code sharing platform.

- **[GitLab](https://gitlab.com/fabricio.boppre/)**: Profile on GitLab, another source code sharing platform.

- **[LinkedIn](https://www.linkedin.com/in/fabricioboppre)**: Profile on LinkedIn.

- **[Workana](https://www.workana.com/freelancer/2ee30a7bbe77afa605567a0cd339030c)**: Profile on Workana.

- **[Upwork](https://www.upwork.com/freelancers/~013df26040f925b7c9)**: Profile on Upwork.

- **[Freelancer.com](https://www.freelancer.com/u/fabricioboppre)**: Profile on Freelancer.com.

- **[Kickstarter](https://www.kickstarter.com/profile/fabricioboppre)**: Projects I support or have supported on Kickstarter.
