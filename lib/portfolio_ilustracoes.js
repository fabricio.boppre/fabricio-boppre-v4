import fs from "fs";
import path from "path";
import matter from "gray-matter";

export async function getIlustracoesParaPortfolio(locale) {
  // Let's iterate through the files to create the illustrations list:
  // - https://nodejs.org/docs/latest/api/path.html
  const illustrationsPath = path.join(
    "content",
    "portfolio_ilustracoes",
    `${locale}`
  );
  var files = fs.readdirSync(illustrationsPath);
  // - Filter to include only * .md files (and avoid files like .DS_Stores):
  files = files.filter((file) => file.includes(".md"));
  var illustrations = [];
  files.forEach(async (file) => {
    // Let's create an ID for each illustration, based on the filenames:
    const id = file.replace(".md", "");

    // Get the content from the illustration file:
    const sitePath = path.join(
      "content",
      "portfolio_ilustracoes",
      `${locale}`,
      `${file}`
    );
    const fileContent = fs.readFileSync(sitePath, "utf8");

    // Use gray-matter to extract the illustration data (the YAML meta data) from the file:
    // - https://www.npmjs.com/package/gray-matter
    // - https://yaml.org/
    const matterResult = matter(fileContent, { excerpt: false });

    // Add the note to the array:
    illustrations.push({
      id: id,
      title: matterResult.data.title,
      date: matterResult.data.date,
      description: matterResult.data.description,
      image_thumbnail: matterResult.data.image_thumbnail,
      image_large: matterResult.data.image_large,
    });
  });

  return illustrations;
}
