export function t(translationsLibrary, locale, title) {
  const text = translationsLibrary
    .find((item) => item.title == title)
    .translations.find((item) => item.locale === locale).value;
  return text;
}
