import fs from "fs";
import path from "path";
import matter from "gray-matter";

export async function getSitesParaPortfolio(locale) {
  // Let's iterate through the files to create the sites list:
  // - https://nodejs.org/docs/latest/api/path.html
  const sitesPath = path.join("content", "portfolio_websites", `${locale}`);
  var files = fs.readdirSync(sitesPath);
  // - Filter to include only * .md files (and avoid files like .DS_Stores):
  files = files.filter((file) => file.includes(".md"));
  var sites = [];
  files.forEach(async (file) => {
    // Let's create an ID for each site, based on the filenames:
    const id = file.replace(".md", "");

    // Get the content from the site file:
    const sitePath = path.join(
      "content",
      "portfolio_websites",
      `${locale}`,
      `${file}`
    );
    const fileContent = fs.readFileSync(sitePath, "utf8");

    // Use gray-matter to extract the site data (the YAML meta data) from the file:
    // - https://www.npmjs.com/package/gray-matter
    // - https://yaml.org/
    const matterResult = matter(fileContent, { excerpt: false });

    // Add the note to the array:
    sites.push({
      id: id,
      title: matterResult.data.title,
      date: matterResult.data.date,
      description: matterResult.data.description,
      status: matterResult.data.status,
      url: matterResult.data.url,
      image: matterResult.data.image,
    });
  });

  return sites;
}
