import fs from "fs";
import path from "path";
import matter from "gray-matter";
import { remark } from "remark";
import html from "remark-html";

export async function getPage(section, locale, page) {
  // Get the content from the page file:
  // - https://nodejs.org/docs/latest/api/path.html
  const pagePath = path.join(
    "content",
    `${section}`,
    `${locale}`,
    `${page}.md`
  );
  const fileContent = fs.readFileSync(pagePath, "utf8");

  // Use gray-matter to extract the note data (the main content and the YAML meta data) from the file:
  // - https://www.npmjs.com/package/gray-matter
  // - https://yaml.org/
  const matterResult = matter(fileContent, { excerpt: false });

  // Use remark to convert markdown into HTML string:
  // - https://www.npmjs.com/package/remark
  // - https://www.npmjs.com/package/remark-html
  const processedContent = await remark()
    .use(html)
    .process(matterResult.content);
  const contentHtml = processedContent.toString();

  return {
    contentHtml,
    ...matterResult.data,
  };
}
