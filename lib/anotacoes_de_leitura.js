import fs from "fs";
import path from "path";
import matter from "gray-matter";
import html from "remark-html";
import { Feed } from "feed";
import { remark } from "remark";
import { t } from "lib/localization/localization";
import translationsLibrary from "lib/localization/places/anotacoes_de_leitura.json";

// Function that creates the XML file for the RSS feed:
export async function generateRSS(locale, notesData) {
  // Preparing some variables:
  const siteURL = process.env.basicURL;
  const author = {
    name: "Fabricio C. Boppré",
    email: "fabricio.boppre@gmail.com",
    link: siteURL
  };
  
  // Creating basic feed info:
  const feed = new Feed({
    title: "fabricio c. boppré | " + t(translationsLibrary, locale, "title"),
    description: t(translationsLibrary, locale, "meta_description"),
    id: siteURL,
    link: `${siteURL}${locale}/anotacoes_de_leitura`,
    language: locale,
    favicon: siteURL + "favicon.ico",
    feedLinks: {
      rss2: `${siteURL}/feed-anotacoes-de-leitura-${locale}.xml`
    },
    author
  });

  // Sort the notes by date:
  // - If the date is null we change it to 0 for ordering to work.
  const sortedNotes = notesData.sort((a, b) => {
    if ((a.date === null ? "0" : a.date) < (b.date === null ? "0" : b.date)) {
      return 1;
    } else {
      return -1;
    }
  });

  // Generating all the feed itens:
  sortedNotes.forEach(note => {
    feed.addItem({
      title: note.title,
      id: note.id,
      link: `${siteURL}${locale}/anotacoes_de_leitura`,
      description: note.credit,
      content: note.content + note.credit,
      author: [author],
      date: new Date(note.date),
    });
  });

  // Creating the feed file:
  fs.writeFileSync(`./public/feed-anotacoes-de-leitura-${locale}.xml`, feed.rss2());
}

// Function that provides the reading notes:
export async function getAnotacoesDeLeitura(locale) {
  // Let's iterate through the files to create the notes list:
  // - https://nodejs.org/docs/latest/api/path.html
  const notesPath = path.join("content", "anotacoes_de_leitura", `${locale}`);
  var files = fs.readdirSync(notesPath);
  // - Filter to include only * .md files (and avoid files like .DS_Stores):
  files = files.filter((file) => file.includes(".md"));
  var notes = [];
  // files.forEach(async (file) => {
  for await (const file of files) {
    // Let's create an ID for each note, based on the filenames:
    const id = file.replace(".md", "");

    // Get the content from the note file:
    const notePath = path.join(
      "content",
      "anotacoes_de_leitura",
      `${locale}`,
      `${file}`
    );
    const fileContent = fs.readFileSync(notePath, "utf8");

    // Use gray-matter to extract the note data (the main content and the YAML meta data) from the file:
    // - https://www.npmjs.com/package/gray-matter
    // - https://yaml.org/
    const matterResult = matter(fileContent, { excerpt: false });

    // Use remark to convert markdown (content and credit) into HTML strings:
    // - https://www.npmjs.com/package/remark
    // - https://www.npmjs.com/package/remark-html
    const processedContent = await remark()
      .use(html)
      .process(matterResult.content);
    const contentHtml = processedContent.toString();
    const processedCredit = await remark()
      .use(html)
      .process(matterResult.data.credit);
    const creditHtml = processedCredit.toString();

    // Convert the date to international date format (ISO):
    // - If there isn't a date on the note, we send null;
    // - https://www.w3.org/QA/Tips/iso-date
    // - https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Date/toISOString
    const dateISO =
      matterResult.data.date !== undefined
        ? matterResult.data.date.toISOString().substring(0, 10)
        : null;

    // Add the note to the array:
    notes.push({
      id: id,
      title: matterResult.data.title,
      credit: creditHtml,
      date: dateISO,
      content: contentHtml,
    });
  };

  return notes;
}
